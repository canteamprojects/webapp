$(".service-button").click(function() {
  var id = $(this).data("id");
  $(".modal-body").load(siteUrl + "index.php/modal/service/" + id, function(){
      $('.select2').select2();
  });
});

$(".service-insert-button").click(function() {
  $(".modal-body").load(siteUrl + "index.php/modal/service/", function(){
      $('.select2').select2();
  });
});


$(".service-delete-button").click(function() {
    var id = $(this).data("id");
    window.location.href = siteUrl + "index.php/modal/deleteService/" + id;
});

$(".product-delete-button").click(function() {
  var id = $(this).data("id");
  window.location.href = siteUrl + "index.php/modal/deleteProduct/" + id;
});

$(".product-button").click(function() {
  var id = $(this).data("id");
  $(".modal-body").load(siteUrl + "index.php/modal/product/" + id);
});

$(".product-insert-button").click(function() {
  $(".modal-body").load(siteUrl + "index.php/modal/product/");
});
