'use strict';

const fadeDuration = 1000;    //millis (default=400)

Number.prototype.pad = function(size)
{
	let s = String(this);
	while (s.length < (size || 2)) {s = "0" + s;}
	return s;
}

String.prototype.capitalize = function()
{
	return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
}

class Order
{
	constructor(code)
	{
		this.code = code;
		this.drawCard();
		this.getInfo();
	}

	drawCard()
	{
		let $container = $("#cardContainer");

		let $card = $(	'<div class="card shadow mt-1">' +
							'<div class="card-header" style="padding: 7px 10px;">' +
								'<div class="col-auto"><h3 class="text-white">Code ' + this.code + '</h3></div>' +
							'</div>' +
							'<div class="card-body">' +
								'<div class="state-container">' +
									'<div class="align-self-center text-center">' +
										'<svg version="1.1"' +
										' xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="60px" height="60px" viewBox="0 0 50 50" xml:space="preserve">' +
											'<path fill="#007bff" d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">' +
												'<animateTransform attributeType="xml"' +
												'  attributeName="transform"' +
												'  type="rotate"' +
												'  from="0 25 25"' +
												'  to="360 25 25"' +
												'  dur="0.9s"' +
												'  repeatCount="indefinite"/>' +
											'</path>' +
										'</svg>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>');

		this.$card = $card;
		$card.prependTo($container).fadeIn(fadeDuration);
	}

	getInfo()
	{
		let that = this;
		let today = new Date().toISOString().split("T")[0];
		//today = '2021-05-11';
		let $title = that.$card.find(".card-header");
		let $container = that.$card.find(".state-container");

		jQuery.getJSON({
			type: 'GET',
			url : base_url + 'index.php/API/getOrderByCode/' + today + '/' + this.code,
		})
		.done(function (data)
		{
			console.log(data);
			$title.html('<h3 class="text-white">Code ' + that.code + " - " + data.surname.capitalize() + " " + data.name.capitalize() + '</h3>');

			let html = '';
			html += '<div class="row mb-2">';
			html += 	'<div class="col-auto align-self-left text-left">';
			html += 		'<b>N. Ordine:</b> #' + data.id + '<br>';
			html += 		'<b>Round:</b> ' + data.startTime + ' - ' + data.endTime + '<br>';
			html += 		'<b>Totale:</b> ' + data.total + '€<br>';
			html += 	'</div>';
			html += 	'<div class="col-auto ml-auto align-self-right text-right">';
			if(data.state === 'Inserted' || data.state === 'Confirmed')
			{
				html += '<h5><span class="badge badge-warning">Non ancora pagato</span></h5>';
				html += '<button type="button" class="btn btn-success">';
				html += 	'<i class="fas fa-cash-register" style="margin-right: 8px"></i>';
				html += 	'Pagamento Ricevuto';
				html += '</button>';
			}
			else if (data.state === 'Paid')
			{
				html += '<h5><span class="badge badge-success">Pagato da ritirare</span></h5>';
				html += '<button type="button" class="btn btn-success">';
				html += 	'<i class="fas fa-shopping-bag" style="margin-right: 8px"></i>';
				html += 	'Ordine Ritirato';
				html += '</button>';
			}
			else //if (data.state === 'PickedUp')
			{
				html += '<h5><span class="badge badge-danger">Già ritirato</span></h5>';
			}
			html += 	'</div>';
			html += '</div>';
			html += '<div class="row">';
			for(let i in data.items) {
				let item = data.items[i];
				html += '<div class="col-auto">'
				html += 	'<div class="card card-block text-right" style="background-color: rgba(0,0,0,.025)">'
				html += 		'<div id="p_4" class="card-body">'
				html += 			'<span class="user-select-none">'
				html += 				'<h5>' + item.productName + '</h5>'
				html += 				'x' + item.quantity + '<br>'
				html += 			'</span>'
				html += 		'</div>'
				html += 	'</div>'
				html += '</div>'
			}
			html += '</div>';
			$container.html(html);

			if(data.state === 'Inserted' || data.state === 'Confirmed')
			{
				$container.find(".btn").click(function () {
					jQuery.getJSON({
						type: 'GET',
						url: base_url + 'index.php/API/setOrderPaidWithCash/' + data.id,
					})
					.always(function () {
						that.getInfo();
					})
				});
			}
			else if (data.state === 'Paid')
			{
				$container.find(".btn").click(function () {
					jQuery.getJSON({
						type: 'GET',
						url: base_url + 'index.php/API/pickupOrder/' + data.id,
					})
					.always(function () {
						that.getInfo();
					})
				});
			}
		})
		.fail(function ()
		{
			let html = '';
			html += '<div class="text-center">';
			html += 	'<h3 class="mb-0">';
			html += 		'<i class="fas fa-times text-danger" style="margin-right: 8px"></i> Codice non valido';
			html += 	'</h3>';
			html += '</div>';
			$container.html(html);
		});
	}
}


let orders = [];
function callback(code)
{
	$('#indicazione').text("");
	for(let i = 0; i < orders.length; i++)
	{
		let order = orders[i];
		if(order.code === code)
		{
			order.$card.remove();
		}
	}
	orders.push(new Order(code));
}

/*
OnLoad
*/
$(function ()
{
	setCodeReadCallback(callback);
});
