'use strict';

let currentCode = "";
let codeReadCallback = function (code) { console.log(code); };
function setCodeReadCallback(f) { codeReadCallback = f; }
function getCodeReadCallback() { return codeReadCallback; }

document.addEventListener('keydown', function(event) {
    let key = conversionTable[event.code];
    if (key !== undefined)
    {
        currentCode += key;
    }
    else if (event.code === "Escape" || event.code === "Backspace")
    {
        currentCode = "";
    }

    if (currentCode.length === 4)
    {
        codeReadCallback(currentCode);
        currentCode = "";
    }

    let text = 'Codice: ';
    for(let i = currentCode.length; i < 4; i++)
    {
        text += '_';
    }
    text += currentCode;
    $('#code').text(text)
});

let conversionTable =
{
    "Digit0": "0",
    "Digit1": "1",
    "Digit2": "2",
    "Digit3": "3",
    "Digit4": "4",
    "Digit5": "5",
    "Digit6": "6",
    "Digit7": "7",
    "Digit8": "8",
    "Digit9": "9",
    "Numpad0": "0",
    "Numpad1": "1",
    "Numpad2": "2",
    "Numpad3": "3",
    "Numpad4": "4",
    "Numpad5": "5",
    "Numpad6": "6",
    "Numpad7": "7",
    "Numpad8": "8",
    "Numpad9": "9",
    // "KeyA": "A",
    // "KeyB": "B",
    // "KeyC": "C",
    // "KeyD": "D",
    // "KeyE": "E",
    // "KeyF": "F",
};


