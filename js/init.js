var siteUrl = "http://localhost/webapp/";

$(function () {
    $(".dataTable").DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false,
      "scrollX": true,
      "columnDefs": [{ "width": "5%", "targets": 0 }, { "width": "5%", "targets": 1 }]
    });
    $(".dataTable2").DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': false,
        'iDisplayLength': 25,
        "scrollX": true,
        "columnDefs": [{"width": "20%", "targets": 2}]
    })
});
