<?php

namespace helpers;

class usersHelper
{
    public static function userPictureOrEmpty($immagineBase64)
    {
        if (empty($immagineBase64)) { ?>
            <img class="profile-user-img img-fluid img-circle"
                 src="<?php echo base_url('images'); ?>/blank-profile-picture.png"
                 alt="User profile picture">
            <?php
        } else { ?>
            <img class="profile-user-img img-fluid img-circle"
                 src='data:image/jpeg;base64, <?php echo $immagineBase64; ?>'" alt="User profile picture">
            <?php
        }
    }
}