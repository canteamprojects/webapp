<?php

namespace helpers;

class ordersHelper
{
    public static function orderStateBadge($orderState)
    {
        if ($orderState == 'Inserted') { ?>
            <span class="badge badge-secondary">Inserito</span>
            <?php
        } elseif ($orderState == 'Confirmed') {
            ?>
            <span class="badge badge-warning">Da ritirare</span>
            <?php
        } elseif ($orderState == 'Paid') {
            ?>
            <span class="badge badge-primary">Da ritirare (già pagato)</span>
            <?php
        } else {
            ?> <!-- PickedUp -->
            <span class="badge badge-success">Completato</span>
            <?php
        }
    }
}