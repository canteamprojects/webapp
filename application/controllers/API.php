<?php
defined('BASEPATH') or exit('No direct script access allowed');

class API extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function getOrderByCode(string $date, int $code)
    {
        $order = $this->orders->getOrderByCode($code, $date);

        $order['immagine'] = null; //Breaks the json_encode
        $order['items'] = $this->orders->getOrderItems($order['id']);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($order))
            ->set_status_header(200);
    }
    public function setOrderPaidWithCash(int $orderId)
    {
        $this->orders->setOrderPaidWithCash($orderId);
        $this->output->set_status_header(200);
    }
    public function pickupOrder(int $orderId)
    {
        $this->orders->pickupOrder($orderId);
        $this->output->set_status_header(200);
    }
}
