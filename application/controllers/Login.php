<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{

	/**
	* Index Page for this controller.
	*
	* Maps to the following URL
	* 		http://example.com/index.php/welcome
	*	- or -
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see https://codeigniter.com/user_guide/general/urls.html
	*/
	public function index($error = "")
	{
		$this->load->model('LoginControll');
		if($this->LoginControll->checkLogin()){
			redirect("dashboard/");
		}

        $username = $this->input->get('username');
        if($username == null)
            $username = "";
		$this->load->view('login',["message" => $error, "username" => $username]);
	}

    public function register(){
        $this->load->model('Users');

        if ($this->input->post()) {

            if(isset($_FILES['photo']['tmp_name']) && $_FILES['photo']['tmp_name'] != "")
                $image = file_get_contents($_FILES['photo']['tmp_name']);
            else
                $image = file_get_contents(FCPATH."images\avatar5.png");

            $email = $this->input->post('email');

            $login = (strtolower($this->input->post('name')[0])) . "" . (strtolower($this->input->post('surname')));
            $user = array('login' => $login, 'password' => $this->input->post('password'), 'surname' => $this->input->post('surname'), 'name' => $this->input->post('name'), 'isAdmin' => 0,
                'indirizzo' => $this->input->post('address'), 'telefono' => $this->input->post('phone'), 'dataDiNascita' => $this->input->post('date'), 'immagine' => $image, 'email' => $email);
            $this->Users->inserUser($user);

            $this->email->HTMLEmail("Registrazione Effettuata - Canteen Reservation", $email, 'mail/DoneRegistration', ['username' => $login])->send();

            $this->index("");
        }
        else
        {
            $this->load->view('register');
        }
    }
  /**
	* Funzione di controllo login
	*/
	public function doLogin(){
		$username=$this->input->post("username");
		$password=$this->input->post("password");
		$this->load->model('loginControll');
		if($this->loginControll->login($username,$password)){
			redirect("dashboard/");
		}else{
			$this->index("Login non riuscito");
		}
	}

  public function logout()
	{
		$this->load->model('loginControll');
        $this->loginControll->logout();
        redirect("login/");
	}
}
