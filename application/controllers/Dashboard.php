<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct()
    {
        parent::__construct();
        $this->viewFolder = "";
        $this->model = "Menu";

        $this->breadcrumb->add('home', 'Home', '/', '');
        $this->breadcrumb->add('contactUs', 'Contattaci', 'dashboard/contactUs', 'home');
        $this->breadcrumb->add('profile', 'Storico Ordini', 'dashboard/profile', 'home');
        $this->breadcrumb->add('invoice', 'Riepilogo Ordine', 'dashboard/invoice', 'home');
        $this->breadcrumb->add('newOrder', 'Nuovo Ordine', 'dashboard/newOrder', 'home');

        $this->breadcrumb->add('ordersAdmin', 'Ordini', 'dashboard/orderListAdmin', 'home');
        $this->breadcrumb->add('distributionAdmin', 'Distribuzione', 'dashboard/distributionAdmin', 'home');
        $this->breadcrumb->add('menuAdmin', 'Gestione Menù', 'dashboard/menuAdmin', 'home');
        $this->breadcrumb->add('userAdmin', 'Utenti', 'dashboard/usersAdmin', 'home');

    }

    public function index()
    {
        if (isset($_SESSION['administrator']) && $_SESSION['administrator']) {
            redirect("dashboard/orderlistAdmin");
        } else $this->load->template("user/dashboard", ['breadcrumbId' => 'home']);
    }

    public function contactUs()
    {
        if ($this->input->post()) {
            $user = $this->loginControll->get_user();
            $username = $user->login;
            $email = $user->email;

            $object = $this->input->post("oggetto");
            $content = $this->input->post("messaggio");

            $text = "E-Mail: " . $email . "\nNome: " . $username . "\nMessaggio: " . $content;

            $this->email->TextEmail($object, MAIL_FROM, $text)->send();
            $this->utility->addsuccess("La mail è stata inviata correttamente");
        }
        $this->load->template('user/contactUs', ['breadcrumbId' => 'contactUs']);
    }

    public function profile($userId = null)
    {
        $localUser = $this->loginControll->get_user();
        if (isset($userId)) {
            if (!(isset($_SESSION['administrator']) && $_SESSION['administrator']))
                throw new Exception('Must be admin to access another user profile.');
        } else {
            $userId = $localUser->id;
        }

        $this->load->model("users", "users_model");
        $this->load->model("orders", "orders_model");

        $data['user'] = $this->users_model->selectUser($userId);
        $data['orders'] = $this->orders_model->getUserOrders($userId);
        $data['breadcrumbId'] = 'profile';

        $this->load->template('user/profile', $data);
    }

    public function newOrder(string $date = null, int $slot = null)
    {
        $user = $this->loginControll->get_user();

        $data['selectDate'] = $date === null || $slot === null;
        $data['roundsSlots'] = $this->roundsSlots->getAll();
        $data['productCategories'] = $this->productCategories->getAll();
        if (!$data['selectDate']) {
            $data['menuOfDay'] = $this->menu->getMenuForDate($date);
            $data['date'] = $date;
            $data['slot'] = $data['roundsSlots'][array_search($slot, array_column($data['roundsSlots'], 'id'))];

            if (count($data['menuOfDay']) == 0) {
                $data['selectDate'] = true;
                $this->utility->addError("Non è stato impostato un menu per il giorno selezionato");
            }
        }
        $data['breadcrumbId'] = 'newOrder';


        $this->load->template('user/newOrder', $data);
    }

    public function postOrder()
    {
        $user = $this->loginControll->get_user();

        $order = json_decode(file_get_contents('php://input'));
        $order->userId = $this->loginControll->get_user()->id;
        $inserted = $this->orders->insertOrder($order);
        $orderId = $inserted['id'];
        $code = $inserted['code'];

        $dates = explode("-", $order->date);//2021-05-11
        $itDate = $dates[2]."-".$dates[1]."-".$dates[0];//11-05-2021

        /*$text = "Codice: " . $code;
        $this->email->TextEmail("Prenotazione Mensa", $user->email, $text)->send();*/
        $this->email->HTMLEmail("Prenotazione Mensa ".$itDate, $user->email, 'mail/SecretCode', ['code' => $code])->send();

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array('orderId' => $orderId)))
            ->set_status_header(200);
    }

    public function invoice($orderId)
    {
        $data['orderId'] = $orderId;

        $userId = $this->loginControll->get_user()->id;
        $isAdmin = false;
        if (isset($userId)) {
            $isAdmin = isset($_SESSION['administrator']) && $_SESSION['administrator'];
        }

        $this->load->model("orders", "orders_model");

        $order = $this->orders_model->getOrder($orderId);

        $isOrderUser = $order->userId == $userId;
        if (!$isOrderUser && !$isAdmin)
            throw new Exception('Must be admin to access another user order.');

        $data['order'] = $order;
        $data['canPickup'] = $isAdmin && ($order->state == 'Confirmed' || $order->state == 'Paid');
        $data['canPay'] = $isOrderUser && ($order->state == 'Inserted' || $order->state == 'Confirmed');
        $data['items'] = $this->orders_model->getOrderItems($orderId);
        $data['breadcrumbId'] = 'invoice';

        $this->load->template('user/invoice', $data);
    }

    public function orderListAdmin()
    {
        $this->load->model('Orders');
        $data = array('databaseData' => $this->Orders->getList());
        $data['breadcrumbId'] = 'ordersAdmin';
        $this->load->template('administrator/orderList.php', $data);
    }

    // Riordina la lista secondo certi criteri

    public function orderValueListAdmin()
    {

        $elenco = $this->input->post("elenco");
        $this->load->model('Orders');
        $data = array('databaseData' => $this->Orders->listByValue($elenco));
        $this->load->template('administrator/orderList.php', $data);

    }

// Estrapola dalla lista un ordine particolare

    public function orderElementAdmin()
    {
        $ordine = $this->input->post("ordine");
        $this->load->model('Orders');
        $data = array('databaseData' => $this->Orders->getElement($ordine));
        $this->load->template('administrator/orderList.php', $data);
    }

    public function menuAdmin()
    {
        $this->load->model("Menu");
        $this->load->model("Service");
        $products = $this->Menu->listAllProduct();
        $services = $this->Service->listAllService();
        $breadcrumbId = 'menuAdmin';
        //TODO switch per utenti administrator
        $this->load->template('administrator/menu.php', compact('products', 'services', 'breadcrumbId'));
    }

    public function usersAdmin()
    {
        $this->load->model('Users');
        $data = $this->Users->selectAllUsers();
        $this->load->template('administrator/users.php', array("result" => $data, "breadcrumbId" => 'userAdmin'));
    }

    public function distributionAdmin()
    {
        $data['breadcrumbId'] = 'distributionAdmin';
        $this->load->template('administrator/distribution.php', $data);
    }
}
