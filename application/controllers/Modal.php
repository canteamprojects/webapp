<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Modal extends MY_Controller
{
    public function service($date = '')
    {
        $data = array();
        $this->load->model("service");
        $this->load->model("menu");
        if ($date){
          $data['service'] = $this->service->detailService($date)[0];
          $data['productsSelect'] = $this->menu->productOption($data['service']['date']);
        }
        else{
              $data['productsSelect'] = $this->menu->productOption();
        }
        $this->load->view("modal/service.php", $data);
    }

    public function product($date = "")
    {
        $data = array();
        $this->load->model("menu");
        if($date){
          $data['product'] = $this->menu->detailProduct($date)[0];
          $data['categorySelect'] = $this->menu->categoryProductOption($data['product']['categoryId']);
        }
        else{
            $data['categorySelect'] = $this->menu->categoryProductOption();
        }
        $this->load->view("modal/product.php", $data);
    }

    public function insertService()
    {
        $this->load->model("service");
        $data = $this->input->post();
        $this->service->add($data);
        redirect(base_url() . "index.php/dashboard/menuAdmin");
    }

    public function insertProduct()
    {
        $data = $this->input->post();
        if (isset($data['id'])) $this->utility->updateDb('products', 'id', $data);
        else $this->utility->insertDb('products', $data);
        redirect(base_url() . "index.php/dashboard/menuAdmin");
    }

    public function deleteService($date = "")
    {
        $this->utility->deleteDb('menuItems', 'date', $date);
        redirect(base_url() . "index.php/dashboard/menuAdmin");
    }

    public function deleteProduct($id = "")
    {
        $this->utility->deleteDb('products', 'id', $id);
        redirect(base_url() . "index.php/dashboard/menuAdmin");
    }

    public function pickUpOrder()
    {
        if (!(isset($_SESSION['administrator']) && $_SESSION['administrator']))
            throw new Exception('Must be admin to set an order as picked up.');

        $orderId = $this->input->post('orderId');
        if ($orderId) {
            $this->load->model("orders", "orders_model");
            $this->orders_model->pickupOrder($orderId);
        }
        redirect(base_url() . "index.php/dashboard/invoice/" . $orderId);
    }

    public function payWithCreditCard()
    {
        $orderId = $this->input->post('orderId');
        $owner = $this->input->post('owner');
        $ccn = $this->input->post('ccn');
        $exp = $this->input->post('exp');
        $cvc = $this->input->post('cvc');
        if ($orderId) {
            $this->load->model("orders", "orders_model");
            $this->orders_model->setOrderPaidWithCreditCard($orderId, $owner, $ccn, $exp, $cvc);
        }
        redirect(base_url() . "index.php/dashboard/invoice/" . $orderId);
    }

}
