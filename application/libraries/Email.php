<?php declare(strict_types=1);
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Usage:
//$a = $this->email->TextEmail("TestMail", "123456@stud.unive.it", "Test Body");
//$b = $this->email->HTMLEmail("TestMail", "ypuindwy@sharklasers.com", "testMail", ["name" => "Davide"]);
//$a->send();
//$b->send();

class CI_Email
{
    public function TextEmail(string $subject, string $to, string $body): MY_TextEmail
    {
        return new MY_TextEmail($subject, $to, $body);
    }

    public function HTMLEmail(string $subject, string $to, string $view, array $data): MY_HTMLEmail
    {
        return new MY_HTMLEmail($subject, $to, $view, $data);
    }
}

abstract class MY_EmailAbstract
{
    public $phpMailerObj;

    function __construct(string $subject, string $to) {
        $this->phpMailerObj = new PHPMailer();
        $this->phpMailerObj->IsSMTP();
        $this->phpMailerObj->Mailer = "smtp";
        $this->phpMailerObj->SMTPDebug  = 0;
        $this->phpMailerObj->SMTPAuth   = TRUE;
        $this->phpMailerObj->SMTPSecure = "tls";
        $this->phpMailerObj->SMTPAutoTLS = false;
        $this->phpMailerObj->Host       = MAIL_HOST;
        $this->phpMailerObj->Port       = MAIL_PORT;
        $this->phpMailerObj->Username   = MAIL_USER;
        $this->phpMailerObj->Password   = MAIL_PASS;
        $this->phpMailerObj->SetFrom(MAIL_FROM, "Canteen Reservation");

        $this->phpMailerObj->Subject = $subject;
        $this->phpMailerObj->AddAddress($to);
    }

    function send(): bool
    {
        try {
            return $this->phpMailerObj->send();
        } catch (Exception $e) {
            var_dump($e);
            return false;
        }
    }
}

class MY_TextEmail extends MY_EmailAbstract
{
    function __construct(string $subject, string $to, string $body) {
        parent::__construct($subject, $to);
        $this->phpMailerObj->IsHTML(false);
        $this->phpMailerObj->Body = $body;
    }
}

class MY_HTMLEmail extends MY_EmailAbstract
{
    function __construct(string $subject, string $to, string $view, array $data) {
        parent::__construct($subject, $to);
        $CI =& get_instance();
        $body = $CI->load->view($view, $data, TRUE);
        $this->phpMailerObj->IsHTML(true);
        $this->phpMailerObj->MsgHTML($body);
    }
}