<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Breadcrumb {

    private $breadcrumbs;

    function __construct()
    {
        $this->breadcrumbs = array();
    }

    function add($id, $name, $link, $Parent){
        if(!isset($this->breadcrumbs[$id]))
        {
            $this->breadcrumbs[$id] = new BreadcrumbObj();
            $this->breadcrumbs[$id]->Id = $id;
            $this->breadcrumbs[$id]->Name = $name;
            $this->breadcrumbs[$id]->Link = $link;
            $this->breadcrumbs[$id]->Parent = $Parent;
        }
        return $id;
    }

    function getTitle($id): string
    {
        if(isset($this->breadcrumbs[$id]))
        {
            return $this->breadcrumbs[$id]->Name;
        }
        return "";
    }

    function render($id): string
    {
        $items = "";
        $first = true;
        while(isset($this->breadcrumbs[$id]))
        {
            $breadcrumb = $this->breadcrumbs[$id];

            $item = "";
            if($first)
            {
                $item .= '<li class="breadcrumb-item active">';
                $item .=    $breadcrumb->Name;
                $item .= '</li>';
                $first = false;
            }
            else
            {
                $item .= '<li class="breadcrumb-item">';
                $item .= '<a href='. site_url($breadcrumb->Link).'>'.$breadcrumb->Name.'</a>';
                $item .= '</li>';
            }

            $items = $item . $items; //Push on front Ex -> (Home / Parent 1 / Page callee)

            $id = $breadcrumb->Parent;
        }


        $output  = '<ol class="breadcrumb float-sm-right">';
        $output .= $items;
        $output .= '</ol>';

        return $output;
    }
}

class BreadcrumbObj {
    public $Id;
    public $Name;
    public $Link;
    public $Parent;
}
