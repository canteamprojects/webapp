<?php

require_once("TestBase.php");

class UsersTest extends TestBase
{
    public function testGetUsersFromEmptyDatabase()
    {
        $model = new Users();
        $users = $model->selectAllUsers();
        $this->assertEquals(0, count($users), "Users table should be empty");
    }

    public function testGetUsers()
    {
        $this->fillUsers();
        $this->fillRoundSlots();
        $this->fillOrders();

        $foundUsers = array(
            2 => ["id" => 2, "login" => 'mrossi', "name" => 'Marco', "surname" => 'Rossi', "isAdmin" => '0', "indirizzo" => 'Via Orlanda 1, 30173 Venezia VE', "telefono" => '3494984571', "dataDiNascita" => '1996-04-11', "email" => '881347@stud.unive.it', "ord" => 12, "ordConf" => 2, "ordNonConf" => 6],
            3 => ["id" => 3, "login" => 'gpettinato', "name" => 'Giada', "surname" => 'Pettinato', "isAdmin" => '0', "indirizzo" => 'Via del Miglio 30, 30173 Venezia VE', "telefono" => '3338603102', "dataDiNascita" => '1999-05-01', "email" => 'gpettinato@gmail.com', "ord" => 4, "ordConf" => 2, "ordNonConf" => 1],
            4 => ["id" => 4, "login" => 'abusatto', "name" => 'Alessandro', "surname" => 'Busatto', "isAdmin" => '0', "indirizzo" => 'Viale Giuseppe Garibaldi 155, 30174 Venezia V', "telefono" => '3484857316', "dataDiNascita" => '1998-11-10', "email" => 'abusatto@gmail.com', "ord" => 3, "ordConf" => 2, "ordNonConf" => 0]
        );

        $model = new Users();
        $users = $model->selectAllUsers();
        $this->assertEquals(3, count($users));
        foreach ($users as $user) {
            $id = $user["id"];
            $foundUser = $foundUsers[$id];
            unset($foundUsers[$id]);

            $this->assertEquals($foundUser["login"], $user["login"]);
            $this->assertEquals($foundUser["surname"], $user["surname"]);
            $this->assertEquals($foundUser["name"], $user["name"]);
            $this->assertEquals($foundUser["isAdmin"], $user["isAdmin"]);
            $this->assertEquals($foundUser["indirizzo"], $user["indirizzo"]);
            $this->assertEquals($foundUser["telefono"], $user["telefono"]);
            $this->assertEquals(date($foundUser["dataDiNascita"]), date($user["dataDiNascita"]));
            $this->assertEquals($foundUser["email"], $user["email"]);

            $this->assertEquals($foundUser["ord"], $user["ord"]);
            $this->assertEquals($foundUser["ordConf"], $user["ordConf"]);
            $this->assertEquals($foundUser["ordNonConf"], $user["ordNonConf"]);
        }

        $this->assertEmpty($foundUsers);
    }

    public function testRegister()
    {
        $model = new Users();

        $user = array(
            'login' => 'user_login_123',
            'password' => 'very_secure_password',
            'surname' => 'Rossi',
            'name' => 'Mario',
            'isAdmin' => 0,
            'indirizzo' => 'via torino 1 30170 Mestre VE',
            'telefono' => '399 1234 567',
            'dataDiNascita' => '1990-03-30',
            'immagine' => Null,
            'email' => 'email@domain.abc');

        $model->inserUser($user);

        $users = $model->selectAllUsers();
        $this->assertSame(1, count($users), "There should be only one user");
        $insertedUser = $users[0];
        $id = $insertedUser['id'];
        $insertedUser = $model->selectUser($id);
        $this->assertNotNull($insertedUser);
        $this->assertEquals('user_login_123', $insertedUser->login);
        $this->assertEquals('Rossi', $insertedUser->surname);
        $this->assertEquals('Mario', $insertedUser->name);
        $this->assertEquals(0, $insertedUser->isAdmin);
        $this->assertEquals('via torino 1 30170 Mestre VE', $insertedUser->indirizzo);
        $this->assertEquals('399 1234 567', $insertedUser->telefono);
        $this->assertEquals(date('1990-03-30'), date($insertedUser->dataDiNascita));
        $this->assertNull($insertedUser->immagine);
        $this->assertEquals('email@domain.abc', $insertedUser->email);
    }

    public function testWrongLogin()
    {
        $this->fillUsers();
        $model = new Users();
        $user = $model->login("abusatto", "123");
        $this->assertNull($user);
    }

    public function testCorrectLogin()
    {
        $this->fillUsers();
        $model = new Users();
        $user = $model->login("abusatto", "123456");
        $this->assertNotNull($user);
        $this->assertEquals(4, $user->id);
        $this->assertEquals("abusatto", $user->login);
    }
}
