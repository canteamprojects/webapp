<?php

require_once("TestBase.php");

class MenuTest extends TestBase
{
    public function testGetAllProductsFromEmptyDatabase()
    {
        $model = new Menu();
        $products = $model->listAllProduct();
        $this->assertEquals(0, count($products), "Products table should be empty");
    }

    private function getExpectedProducts(): array
    {
        return array(
            11 => ["productId" => 11, "name" => "Antipasti", "productName" => 'Arancini di riso', "price" => 2.00, "quantity" => 3],
            12 => ["productId" => 12, "name" => "Antipasti", "productName" => 'Mozzarella in carrozza', "price" => 2.50, "quantity" => 2],
            21 => ["productId" => 21, "name" => "Primi", "productName" => 'Spaghetti alla carbonara', "price" => 8.00, "quantity" => 5],
            22 => ["productId" => 22, "name" => "Primi", "productName" => 'Lasagne alla bolognese', "price" => 9.00, "quantity" => 5],
            23 => ["productId" => 23, "name" => "Primi", "productName" => 'Trofie al pesto', "price" => 9.00, "quantity" => 4],
            31 => ["productId" => 31, "name" => "Secondi", "productName" => 'Orata al forno', "price" => 12.00, "quantity" => 0],
            32 => ["productId" => 32, "name" => "Secondi", "productName" => 'Polpette al forno', "price" => 9.00, "quantity" => 4],
            33 => ["productId" => 33, "name" => "Secondi", "productName" => 'Frittura mista', "price" => 15.00, "quantity" => 2],
            41 => ["productId" => 41, "name" => "Contorni", "productName" => 'Patate al forno', "price" => 6.00, "quantity" => 2],
            42 => ["productId" => 42, "name" => "Contorni", "productName" => 'Verdure grigliate', "price" => 5.00, "quantity" => 3],
            51 => ["productId" => 51, "name" => "Dolci", "productName" => 'Tiramisu', "price" => 4.00, "quantity" => 8],
            52 => ["productId" => 52, "name" => "Dolci", "productName" => 'Crostata di fragole', "price" => 4.00, "quantity" => 6],
            61 => ["productId" => 61, "name" => "Bibite", "productName" => 'Acqua naturale', "price" => 1.00, "quantity" => 18],
            62 => ["productId" => 62, "name" => "Bibite", "productName" => 'Acqua gassata', "price" => 1.00, "quantity" => 18]
        );
    }

    private function compareProduct(array $expectedProduct, array $actualProduct): void
    {
        $this->assertEquals($expectedProduct["productName"], $actualProduct["productName"]);
        $this->assertEquals($expectedProduct["price"], $actualProduct["price"]);
        $this->assertEquals($expectedProduct["quantity"], $actualProduct["quantity"]);
        $this->assertEquals($expectedProduct["name"], $actualProduct["name"]);
    }

    private function compareProducts(array $expectedProducts, array $actualProducts): void
    {
        $this->assertSameSize($expectedProducts, $actualProducts);

        foreach ($actualProducts as $prod) {
            $id = $prod["productId"];
            $foundProd = $expectedProducts[$id];
            unset($expectedProducts[$id]);
            $this->compareProduct($foundProd, $prod);
        }

        $this->assertEmpty($expectedProducts);
    }

    public function testGetAllProducts()
    {
        $this->fillProducts();

        $expectedProducts = $this->getExpectedProducts();

        $model = new Menu();
        $products = $model->listAllProduct();

        $this->compareProducts($expectedProducts, $products);
    }

    public function testAddProduct()
    {
        $this->fillProducts();
        $model = new Menu();

        $productToAddId = 63;
        $productToAdd = ["id" => $productToAddId, "productName" => "Vino", "categoryId" => 6, "quantity" => 3, "price" => 2.50];
        $model->addProduct($productToAdd);
        $productToAdd["name"] = "Bibite";
        $productToAdd["productId"] = $productToAdd["id"];

        $expectedProducts = $this->getExpectedProducts();
        $expectedProducts[$productToAddId] = $productToAdd;

        $products = $model->listAllProduct();

        $this->compareProducts($expectedProducts, $products);
    }

    public function testRemoveProduct()
    {
        $this->fillProducts();
        $model = new Menu();

        $productToRemoveId = 31;
        $model->removeProduct($productToRemoveId);

        $expectedProducts = $this->getExpectedProducts();
        unset($expectedProducts[$productToRemoveId]);

        $products = $model->listAllProduct();

        $this->compareProducts($expectedProducts, $products);
    }

    public function testProductDetail()
    {
        $this->fillProducts();
        $model = new Menu();

        $productToCheckId = 51;
        $expectedProduct = ["productId" => $productToCheckId, "productName" => "Tiramisu", "categoryId" => 5, "name" => "Dolci", "quantity" => 8, "price" => 4.0];

        $foundProducts = $model->detailProduct($productToCheckId);
        $this->compareProduct($expectedProduct, $foundProducts[0]);
    }
}
