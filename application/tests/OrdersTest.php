<?php

require_once("TestBase.php");

class OrdersTest extends TestBase
{
    public function testGetOrdersFromEmptyDatabase()
    {
        $model = new Orders();
        $orders = $model->getUserOrders(0);
        $this->assertEquals(0, count($orders));
    }

    public function testGetOrdersFromUserThatMadeNone()
    {
        $this->fillUsers();
        $this->fillRoundSlots();
        $this->fillProducts();
        $this->fillOrders();
        $this->fillOrderItems();

        $model = new Orders();
        $orders = $model->getUserOrders(1); // admin
        $this->assertEquals(0, count($orders));
    }

    public function testGetOrdersFromUser()
    {
        $this->fillUsers();
        $this->fillRoundSlots();
        $this->fillProducts();
        $this->fillOrders();
        $this->fillOrderItems();

        $model = new Orders();
        $actualOrders = $model->getUserOrders(4);

        $expectedOrders = $this->getExpectedOrders();
        $this->compareOrders($expectedOrders, $actualOrders);
    }

    private function getExpectedOrders(): array
    {
        return array(
            3 => ["id" => 3, "userId" => 4, "total" => 21.60, "state" => 'PickedUp', "paymentType" => 'CreditDebitCard', "slotId" => 8, "date" => '2018-05-03', "code" => 9756],
            6 => ["id" => 6, "userId" => 4, "total" => 11.70, "state" => 'Confirmed', "paymentType" => 'CreditDebitCard', "slotId" => 4, "date" => '2018-06-02', "code" => 7513],
            19 => ["id" => 19, "userId" => 4, "total" => 19.60, "state" => 'Paid', "paymentType" => 'CreditDebitCard', "slotId" => 0, "date" => '2021-05-11', "code" => 4311]
        );
    }

    private function compareOrder(array $expected, array $actual): void
    {
        $this->assertEquals($expected["id"], $actual["id"]);
        //$this->assertEquals($expected["userId"], $actual["userId"]);
        $this->assertEquals($expected["total"], $actual["total"]);
        $this->assertEquals($expected["state"], $actual["state"]);
        $this->assertEquals($expected["paymentType"], $actual["paymentType"]);
        //$this->assertEquals($expected["slotId"], $actual["slotId"]);
        $this->assertEquals($expected["date"], $actual["orderDate"]);
        $this->assertEquals($expected["code"], $actual["code"]);
    }

    private function compareOrders(array $expected, array $actual): void
    {
        $this->assertSameSize($expected, $actual);

        foreach ($actual as $order) {
            $id = $order["id"];
            $foundSlot = $expected[$id];
            unset($expected[$id]);
            $this->compareOrder($foundSlot, $order);
        }

        $this->assertEmpty($expected);
    }

    public function testGetOrderFromUser()
    {
        $this->fillUsers();
        $this->fillRoundSlots();
        $this->fillProducts();
        $this->fillOrders();
        $this->fillOrderItems();

        $model = new Orders();
        $actualOrder = $model->getOrder(6);

        $expectedOrders = $this->getExpectedOrders();
        $expectedOrder = $expectedOrders[6];

        $this->assertEquals($expectedOrder["userId"], $actualOrder->userId);
        $this->assertEquals($expectedOrder["total"], $actualOrder->total);
        $this->assertEquals($expectedOrder["state"], $actualOrder->state);
        $this->assertEquals($expectedOrder["paymentType"], $actualOrder->paymentType);
        $this->assertEquals($expectedOrder["slotId"], $actualOrder->slotId);
        $this->assertEquals($expectedOrder["date"], $actualOrder->date);
        $this->assertEquals($expectedOrder["code"], $actualOrder->code);
    }

    public function testGetOrderItemsFromUser()
    {
        $this->fillUsers();
        $this->fillRoundSlots();
        $this->fillProducts();
        $this->fillOrders();
        $this->fillOrderItems();

        $model = new Orders();
        $actualOrderItems = $model->getOrderItems(6);
        $expectedOrderItems = $this->getExpectedOrderItems();

        $this->assertSameSize($expectedOrderItems, $actualOrderItems);

        foreach ($actualOrderItems as $actualItem) {
            $id = $actualItem["id"];
            $expectedItem = $expectedOrderItems[$id];
            unset($expectedOrderItems[$id]);

            $this->assertEquals($expectedItem["id"], $actualItem["id"]);
            $this->assertEquals($expectedItem["orderId"], $actualItem["orderId"]);
            $this->assertEquals($expectedItem["productId"], $actualItem["productId"]);
            $this->assertEquals($expectedItem["quantity"], $actualItem["quantity"]);
            $this->assertEquals($expectedItem["unitaryPrice"], $actualItem["unitaryPrice"]);
            $this->assertEquals($expectedItem["total"], $actualItem["total"]);
        }

        $this->assertEmpty($expectedOrderItems);
    }

    private function getExpectedOrderItems(): array
    {
        return array(
            201 => ["id" => 201, "orderId" => 6, "productId" => 21, "quantity" => 1, "unitaryPrice" => 8.00, "total" => 8.00],
            202 => ["id" => 202, "orderId" => 6, "productId" => 31, "quantity" => 1, "unitaryPrice" => 12.00, "total" => 12.00],
            203 => ["id" => 203, "orderId" => 6, "productId" => 62, "quantity" => 1, "unitaryPrice" => 1.00, "total" => 1.00]
        );
    }

    public function testInsertOrder()
    {
        $this->fillUsers();
        $this->fillRoundSlots();
        $this->fillProducts();
        $this->fillOrders();
        $this->fillOrderItems();

        $orderData = json_decode('
{
  "userId": 4,
  "date": "2021-05-13",
  "slot": 7,
  "totalWithDiscount": 22.6,
  "items": [
    {
      "id": 22,
      "qty": 1,
      "price": 9,
      "total": 9
    },
    {
      "id": 33,
      "qty": 1,
      "price": 15,
      "total": 15
    }
  ]
}');
        $model = new Orders();
        $insertedOrderData = $model->insertOrder($orderData);

        $orderId = $insertedOrderData["id"];
        $actualOrder = $model->getOrder($orderId);

        $actualOrderByCode = $model-> getOrderByCode($insertedOrderData["code"], "2021-05-13");
        $this->assertEquals($orderId, $actualOrderByCode["id"]);

        $this->assertEquals(4, $actualOrder->userId);
        $this->assertEquals("2021-05-13", $actualOrder->date);
        $this->assertEquals(22.6, $actualOrder->total);
        $this->assertEquals("Inserted", $actualOrder->state);
        $this->assertEquals(null, $actualOrder->paymentType);
        $this->assertEquals(7, $actualOrder->slotId);

        $actualOrderItems = $model->getOrderItems($orderId);
        $expectedOrderItems = [
            22 => ["productId" => 22, "quantity" => 1, "unitaryPrice" => 9, "total" => 9],
            33 => ["productId" => 33, "quantity" => 1, "unitaryPrice" => 15, "total" => 15]
        ];

        $this->assertSameSize($expectedOrderItems, $actualOrderItems);

        foreach ($actualOrderItems as $actualItem) {
            $id = $actualItem["productId"];
            $expectedItem = $expectedOrderItems[$id];
            unset($expectedOrderItems[$id]);

            $this->assertEquals($orderId, $actualItem["orderId"]);
            $this->assertEquals($expectedItem["productId"], $actualItem["productId"]);
            $this->assertEquals($expectedItem["quantity"], $actualItem["quantity"]);
            $this->assertEquals($expectedItem["unitaryPrice"], $actualItem["unitaryPrice"]);
            $this->assertEquals($expectedItem["total"], $actualItem["total"]);
        }

        $this->assertEmpty($expectedOrderItems);
    }

    public function testPickupOrder()
    {
        $this->fillUsers();
        $this->fillRoundSlots();
        $this->fillProducts();
        $this->fillOrders();
        $this->fillOrderItems();

        $model = new Orders();
        $orderId = 5;
        $insertedOrderData = $model->pickupOrder($orderId);
        $actualOrder = $model->getOrder($orderId);
        $this->assertEquals("PickedUp", $actualOrder->state);
    }

    public function testSetOrderPaidWithCreditCard()
    {
        $this->fillUsers();
        $this->fillRoundSlots();
        $this->fillProducts();
        $this->fillOrders();
        $this->fillOrderItems();

        $model = new Orders();
        $orderId = 9;
        $model->setOrderPaidWithCreditCard($orderId, "Mario Rossi", "1234567891011121", "12/2025", "123");
        $actualOrder = $model->getOrder($orderId);
        $this->assertEquals("Paid", $actualOrder->state);
    }

    public function testSetOrderPaidWithCash()
    {
        $this->fillUsers();
        $this->fillRoundSlots();
        $this->fillProducts();
        $this->fillOrders();
        $this->fillOrderItems();

        $model = new Orders();
        $orderId = 9;
        $model->setOrderPaidWithCash($orderId);
        $actualOrder = $model->getOrder($orderId);
        $this->assertEquals("Paid", $actualOrder->state);
    }
}
