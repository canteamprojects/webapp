<?php

require_once("TestBase.php");

class ProductCategoriesTest extends TestBase
{
    public function testGetAllProductCategoriesFromEmptyDatabase()
    {
        $model = new ProductCategories();
        $categories = $model->getAll();
        $this->assertCount(0, $categories, "Product categories table should be empty");
    }

    private function getExpectedProductCategories(): array
    {
        return array(
            1 => ["id" => 1, "order" => 1, "name" => "Antipasti"],
            2 => ["id" => 2, "order" => 2, "name" => "Primi"],
            3 => ["id" => 3, "order" => 3, "name" => "Secondi"],
            4 => ["id" => 4, "order" => 4, "name" => "Contorni"],
            5 => ["id" => 5, "order" => 5, "name" => "Dolci"],
            6 => ["id" => 6, "order" => 6, "name" => "Bibite"]
        );
    }

    private function compareProductCategory(array $expectedProduct, ProductCategories $actualProduct): void
    {
        $this->assertEquals($expectedProduct["id"], $actualProduct->id);
        $this->assertEquals($expectedProduct["order"], $actualProduct->order);
        $this->assertEquals($expectedProduct["name"], $actualProduct->name);

        $this->assertEquals($expectedProduct["name"], $actualProduct->toString());
    }

    private function compareProductCategories(array $expectedProducts, array $actualProducts): void
    {
        $this->assertSameSize($expectedProducts, $actualProducts);

        foreach ($actualProducts as $prod) {
            $id = $prod->id;
            $foundProd = $expectedProducts[$id];
            unset($expectedProducts[$id]);
            $this->compareProductCategory($foundProd, $prod);
        }

        $this->assertEmpty($expectedProducts);
    }

    public function testGetAllProducts()
    {
        $this->fillProducts();

        $expectedProductCategories = $this->getExpectedProductCategories();

        $model = new ProductCategories();
        $categories = $model->getAll();

        $this->compareProductCategories($expectedProductCategories, $categories);
    }
}
