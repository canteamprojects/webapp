<?php

require_once("TestBase.php");

class ServiceTest extends TestBase
{
    public function testGetAllProductsFromEmptyDatabase()
    {
        $model = new Service();
        $services = $model->listAllService();
        $this->assertEquals(0, count($services));
    }

    private function getExpectedServices(): array
    {
        return array(
            '2021-05-12' => ["date" => '2021-05-12', "products" => "Arancini di riso, Spaghetti alla carbonara, Orata al forno, Patate al forno"],
            '2021-05-13' => ["date" => '2021-05-13', "products" => "Lasagne alla bolognese, Trofie al pesto, Frittura mista"]
        );
    }

    private function compareServices(array $expectedServices, array $actualServices): void
    {
        $this->assertSameSize($expectedServices, $actualServices);

        foreach ($actualServices as $service) {
            $date = $service["date"];
            $foundService = $expectedServices[$date];
            unset($expectedServices[$date]);

            $this->assertEquals($foundService["date"], $service["date"]);

            $expectedProducts = explode(", ", $foundService["products"]);
            $actualProducts = explode(", ", $service["products"]);
            foreach ($expectedProducts as $product) {
                $this->assertTrue(in_array($product, $actualProducts));
            }
            foreach ($actualProducts as $product) {
                $this->assertTrue(in_array($product, $expectedProducts));
            }
        }

        $this->assertEmpty($expectedServices);
    }

    public function testGetAllServices()
    {
        $this->fillProducts();
        $this->fillMenuItems();

        $expectedServices = $this->getExpectedServices();

        $model = new Service();
        $services = $model->listAllService();

        $this->compareServices($expectedServices, $services);
    }

    public function testAddService()
    {
        $this->fillProducts();
        $this->fillMenuItems();
        $model = new Service();

        $serviceDateAddId = '2021-05-15';
        $menuItemsToAdd = [
            "date" => $serviceDateAddId,
            "products" => array(51, 52, 61, 62)
        ];
        $model->add($menuItemsToAdd);

        $expectedServices = $this->getExpectedServices();
        $expectedServices[$serviceDateAddId] = ["date" => $serviceDateAddId, "products" => "Tiramisu, Crostata di fragole, Acqua naturale, Acqua gassata"];

        $services = $model->listAllService();
        $this->compareServices($expectedServices, $services);
    }

    public function testRemoveService()
    {
        $this->fillProducts();
        $this->fillMenuItems();
        $model = new Service();

        $serviceDateAddId = '2021-05-12';

        $expectedServices = $this->getExpectedServices();
        unset($expectedServices[$serviceDateAddId]);

        $model->removeService($serviceDateAddId);

        $services = $model->listAllService();
        $this->compareServices($expectedServices, $services);
    }

    public function testServiceDetail()
    {
        $this->fillProducts();
        $this->fillMenuItems();
        $model = new Service();

        $expectedServices = $this->getExpectedServices();
        unset($expectedServices['2021-05-13']);

        $services = $model->detailService('2021-05-12');
        $this->compareServices($expectedServices, $services);
    }
}
