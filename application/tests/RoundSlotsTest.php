<?php

require_once("TestBase.php");

class RoundSlotsTest extends TestBase
{
    public function testGetAllProductCategoriesFromEmptyDatabase()
    {
        $model = new RoundsSlots();
        $categories = $model->getAll();
        $this->assertCount(0, $categories);
    }

    private function getExpectedRoundSlots(): array
    {
        return array(
            0 => ["id" => 0, "startTime" => '11:00:00', "endTime" => '11:20:00', "numberOfSlots" => 100, "discount" => 0.3, "type" => 'pranzo'],
            1 => ["id" => 1, "startTime" => '11:20:00', "endTime" => '11:40:00', "numberOfSlots" => 100, "discount" => 0.3, "type" => 'pranzo'],
            2 => ["id" => 2, "startTime" => '11:40:00', "endTime" => '12:00:00', "numberOfSlots" => 90, "discount" => 0.2, "type" => 'pranzo'],
            3 => ["id" => 3, "startTime" => '12:00:00', "endTime" => '12:20:00', "numberOfSlots" => 90, "discount" => 0.2, "type" => 'pranzo'],
            4 => ["id" => 4, "startTime" => '12:20:00', "endTime" => '12:40:00', "numberOfSlots" => 80, "discount" => 0.1, "type" => 'pranzo'],
            5 => ["id" => 5, "startTime" => '12:40:00', "endTime" => '13:00:00', "numberOfSlots" => 80, "discount" => 0, "type" => 'pranzo'],
            6 => ["id" => 6, "startTime" => '13:00:00', "endTime" => '13:20:00', "numberOfSlots" => 80, "discount" => 0, "type" => 'pranzo'],
            7 => ["id" => 7, "startTime" => '13:20:00', "endTime" => '13:40:00', "numberOfSlots" => 90, "discount" => 0.1, "type" => 'pranzo'],
            8 => ["id" => 8, "startTime" => '13:40:00', "endTime" => '14:00:00', "numberOfSlots" => 100, "discount" => 0.2, "type" => 'pranzo'],
            9 => ["id" => 9, "startTime" => '19:00:00', "endTime" => '19:20:00', "numberOfSlots" => 100, "discount" => 0.2, "type" => 'cena'],
            10 => ["id" => 10, "startTime" => '19:20:00', "endTime" => '19:40:00', "numberOfSlots" => 100, "discount" => 0.2, "type" => 'cena'],
            11 => ["id" => 11, "startTime" => '19:40:00', "endTime" => '20:00:00', "numberOfSlots" => 90, "discount" => 0.1, "type" => 'cena'],
            12 => ["id" => 12, "startTime" => '20:00:00', "endTime" => '20:20:00', "numberOfSlots" => 80, "discount" => 0, "type" => 'cena'],
            13 => ["id" => 13, "startTime" => '20:20:00', "endTime" => '20:40:00', "numberOfSlots" => 80, "discount" => 0, "type" => 'cena'],
            14 => ["id" => 14, "startTime" => '20:40:00', "endTime" => '21:00:00', "numberOfSlots" => 90, "discount" => 0.1, "type" => 'cena'],
            15 => ["id" => 15, "startTime" => '21:00:00', "endTime" => '21:20:00', "numberOfSlots" => 100, "discount" => 0.2, "type" => 'cena'],
            16 => ["id" => 16, "startTime" => '21:20:00', "endTime" => '21:40:00', "numberOfSlots" => 100, "discount" => 0.3, "type" => 'cena'],
            17 => ["id" => 17, "startTime" => '21:40:00', "endTime" => '22:00:00', "numberOfSlots" => 100, "discount" => 0.3, "type" => 'cena']
        );
    }

    private function compareSlot(array $expected, RoundsSlots $actual): void
    {
        $this->assertEquals($expected["id"], $actual->id);
        $this->assertEquals($expected["startTime"], $actual->startTime);
        $this->assertEquals($expected["endTime"], $actual->endTime);
        $this->assertEquals($expected["numberOfSlots"], $actual->numberOfSlots);
        $this->assertEquals($expected["discount"], $actual->discount);
        $this->assertEquals($expected["type"], $actual->type);
    }

    private function compareSlots(array $expected, array $actual): void
    {
        $this->assertSameSize($expected, $actual);

        foreach ($actual as $slot) {
            $id = $slot->id;
            $foundSlot = $expected[$id];
            unset($expected[$id]);
            $this->compareSlot($foundSlot, $slot);
        }

        $this->assertEmpty($expected);
    }

    public function testGetAllRoundSlots()
    {
        $this->fillRoundSlots();

        $expectedSlots = $this->getExpectedRoundSlots();

        $model = new RoundsSlots();
        $actualSlots = $model->getAll();

        $this->compareSlots($expectedSlots, $actualSlots);

        $slot = $actualSlots[10];
        $this->assertEquals("19:20:00 - 19:40:00", $slot->toString());
    }
}
