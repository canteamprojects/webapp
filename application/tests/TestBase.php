<?php

abstract class TestBase extends TestCase
{
    static private $pdo = null;

    final public function getConnection()
    {
        if (self::$pdo == null) {
            $dsn = 'mysql:dbname=' . DB_NAME . ';host=' . DB_HOST . ':' . DB_PORT;
            $user = DB_USERNAME;
            $password = DB_PASSWORD;
            self::$pdo = new PDO($dsn, $user, $password);
        }
        return self::$pdo;
    }

    final public function setUp(): void
    {
        $pdo = $this->getConnection();

        $pdo->exec("
            CREATE TABLE `users` (
               `id` int(11) NOT NULL AUTO_INCREMENT,
               `login` varchar(45) NOT NULL,
               `password` varchar(45) NOT NULL,
               `surname` varchar(45) NOT NULL,
               `name` varchar(45) NOT NULL,
               `isAdmin` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
               `indirizzo` varchar(45) NOT NULL,
               `telefono` varchar(45) NOT NULL,
               `dataDiNascita` date DEFAULT NULL,
               `immagine` longblob,
               `email` varchar(45) DEFAULT NULL,
               PRIMARY KEY (`id`)
            );");

        $pdo->exec("
            CREATE TABLE `productCategories` (
               `id` int(11) NOT NULL AUTO_INCREMENT,
               `order` int(10) unsigned NOT NULL,
               `name` varchar(45) NOT NULL,
               PRIMARY KEY (`id`)
            );");

        $pdo->exec("
            CREATE TABLE `products` (
               `id` int(11) NOT NULL AUTO_INCREMENT,
               `categoryId` int(11) NOT NULL,
               `productName` varchar(45) NOT NULL,
               `price` decimal(5,2) NOT NULL,
               `quantity` int(11) DEFAULT NULL,
               PRIMARY KEY (`id`),
               KEY `products_productCategories_fk_idx` (`categoryId`),
               CONSTRAINT `products_productCategories_fk` FOREIGN KEY (`categoryId`) REFERENCES `productCategories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            )");

        $pdo->exec("
            CREATE TABLE `menuItems` (
               `productId` int(11) NOT NULL,
               `date` date NOT NULL,
               PRIMARY KEY (`productId`,`date`),
               KEY `menuItems_products_fk_idx` (`productId`),
               CONSTRAINT `menuItems_products_fk` FOREIGN KEY (`productId`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            );");

        $pdo->exec("
            CREATE TABLE `roundsSlots` (
               `id` int(11) NOT NULL,
               `startTime` time NOT NULL,
               `endTime` time NOT NULL,
               `numberOfSlots` int(11) NOT NULL DEFAULT '10',
               `discount` float NOT NULL DEFAULT '0.1',
               `type` enum('pranzo','cena') NOT NULL DEFAULT 'pranzo',
               PRIMARY KEY (`id`)
            );");

        $pdo->exec("
            CREATE TABLE `orders` (
               `id` int(11) NOT NULL AUTO_INCREMENT,
               `userId` int(11) NOT NULL,
               `total` decimal(5,2) NOT NULL,
               `state` enum('Inserted','Confirmed','Paid','PickedUp') NOT NULL,
               `paymentType` enum('Cash','CreditDebitCard') DEFAULT NULL,
               `slotId` int(11) NOT NULL,
               `date` date DEFAULT NULL,
               `code` int(6) NOT NULL,
               PRIMARY KEY (`id`),
               KEY `orders_users_fk_idx` (`userId`),
               KEY `orders_ibfk_1` (`slotId`),
               CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`slotId`) REFERENCES `roundsSlots` (`id`),
               CONSTRAINT `orders_users_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            );");

        $pdo->exec("
            CREATE TABLE `orderItems` (
               `id` int(11) NOT NULL AUTO_INCREMENT,
               `orderId` int(11) NOT NULL,
               `productId` int(11) NOT NULL,
               `quantity` float NOT NULL,
               `unitaryPrice` decimal(5,2) NOT NULL,
               `total` decimal(5,2) NOT NULL,
               PRIMARY KEY (`id`),
               KEY `orderItems_orders_fk_idx` (`orderId`),
               KEY `orderItems_products_fk_idx` (`productId`),
               CONSTRAINT `orderItems_orders_fk` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
               CONSTRAINT `orderItems_products_fk` FOREIGN KEY (`productId`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            );");

        $pdo->exec("
            CREATE TABLE `cardPayments` (
               `orderId` int(11) NOT NULL,
               `cardType` enum('Debit','Credit') NOT NULL,
               `cardNumber` varchar(22) NOT NULL,
               `cardOwner` varchar(100) NOT NULL,
               `cardExpirationDate` varchar(10) NOT NULL,
               `cardCVC` varchar(5) NOT NULL,
               PRIMARY KEY (`orderId`),
               CONSTRAINT `cardPayments_orders_fk` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            );");
    }

    final public function tearDown(): void
    {
        $pdo = $this->getConnection();
        $tableNames = array(
            "users",
            "productCategories",
            "products",
            "menuItems",
            "roundsSlots",
            "orders",
            "orderItems",
            "cardPayments");
        for ($i = count($tableNames) - 1; $i >= 0; $i--) {
            //print "deleting table " . $tableNames[$i] . "\r\n";
            $pdo->exec("DROP TABLE IF EXISTS " . $tableNames[$i]);
        }
    }

    final public function fillUsers()
    {
        $pdo = $this->getConnection();
        $exeResult = $pdo->exec("
INSERT INTO `users` (`id`, `login`, `password`, `surname`, `name`, `isAdmin`, `indirizzo`, `telefono`, `dataDiNascita`, `email`) 
VALUES 
(1,'administrator','adminpassword','Admin','Admin','1','P.za S. Marco 328, 30123 Venezia VE','3504063696','1992-01-05','admin@gmail.com')
,(2,'mrossi','password','Rossi','Marco','0','Via Orlanda 1, 30173 Venezia VE','3494984571','1996-04-11','881347@stud.unive.it')
,(3,'gpettinato','easypassword','Pettinato','Giada','0','Via del Miglio 30, 30173 Venezia VE','3338603102','1999-05-01','gpettinato@gmail.com')
,(4,'abusatto','123456','Busatto','Alessandro','0','Viale Giuseppe Garibaldi 155, 30174 Venezia V','3484857316','1998-11-10','abusatto@gmail.com')        
        ");

        if (!$exeResult) {
            print($pdo->errorInfo());
        }
    }

    final public function fillRoundSlots()
    {
        $pdo = $this->getConnection();
        $exeResult = $pdo->exec("
INSERT INTO `roundsSlots` (`id`, `startTime`, `endTime`, `numberOfSlots`, `discount`, `type`) 
VALUES 
    (0,'11:00:00','11:20:00',100,0.3,'pranzo')
    ,(1,'11:20:00','11:40:00',100,0.3,'pranzo')
    ,(2,'11:40:00','12:00:00',90,0.2,'pranzo')
    ,(3,'12:00:00','12:20:00',90,0.2,'pranzo')
    ,(4,'12:20:00','12:40:00',80,0.1,'pranzo')
    ,(5,'12:40:00','13:00:00',80,0,'pranzo')
    ,(6,'13:00:00','13:20:00',80,0,'pranzo')
    ,(7,'13:20:00','13:40:00',90,0.1,'pranzo')
    ,(8,'13:40:00','14:00:00',100,0.2,'pranzo')
    ,(9,'19:00:00','19:20:00',100,0.2,'cena')
    ,(10,'19:20:00','19:40:00',100,0.2,'cena')
    ,(11,'19:40:00','20:00:00',90,0.1,'cena')
    ,(12,'20:00:00','20:20:00',80,0,'cena')
    ,(13,'20:20:00','20:40:00',80,0,'cena')
    ,(14,'20:40:00','21:00:00',90,0.1,'cena')
    ,(15,'21:00:00','21:20:00',100,0.2,'cena')
    ,(16,'21:20:00','21:40:00',100,0.3,'cena')
    ,(17,'21:40:00','22:00:00',100,0.3,'cena');        
        ");

        if (!$exeResult) {
            print($pdo->errorInfo());
        }
    }

    final public function fillOrders()
    {
        $pdo = $this->getConnection();
        $exeResult = $pdo->exec("
INSERT INTO `orders` (`id`, `userId`, `total`, `state`, `paymentType`, `slotId`, `date`, `code`) 
VALUES (1,2,14.00,'PickedUp','CreditDebitCard',5,'2018-05-01',6584),
       (2,3,15.00,'PickedUp','Cash',6,'2018-05-01',7485),
       (3,4,21.60,'PickedUp','CreditDebitCard',8,'2018-05-03',9756),
       (4,2,19.60,'PickedUp','CreditDebitCard',16,'2018-06-01',7520),
       (5,3,14.40,'Paid','Cash',2,'2018-06-02',4596),
       (6,4,11.70,'Confirmed','CreditDebitCard',4,'2018-06-02',7513),
       (7,2,14.40,'Paid','Cash',11,'2018-06-03',3526),
       (8,2,13.65,'Paid','CreditDebitCard',0,'2021-05-11',7489),
       (9,2,21.00,'Inserted',NULL,0,'2021-05-11',5142),
       (10,2,1.75,'PickedUp','Cash',0,'2021-05-11',5254),
       (11,2,6.30,'Inserted',NULL,0,'2021-05-11',85),
       (12,2,4.20,'PickedUp','CreditDebitCard',0,'2021-05-11',9041),
       (13,2,15.40,'Inserted',NULL,0,'2021-05-13',6855),
       (14,2,16.45,'Inserted',NULL,0,'2021-05-11',9287),
       (15,2,11.20,'Inserted',NULL,0,'2021-05-12',1938),
       (16,3,11.90,'Paid','CreditDebitCard',0,'2021-05-11',2821),
       (17,2,7.00,'Inserted',NULL,0,'2021-05-12',8241),
       (18,3,26.00,'Inserted',NULL,12,'2021-05-12',9678),
       (19,4,19.60,'Paid','CreditDebitCard',0,'2021-05-11',4311);
        ");

        if (!$exeResult) {
            print($pdo->errorInfo());
        }
    }

    final public function fillOrderItems()
    {
        $pdo = $this->getConnection();
        $exeResult = $pdo->exec("
INSERT INTO `orderItems` (`id`, `orderId`, `productId`, `quantity`, `unitaryPrice`, `total`) 
VALUES (101,3,11,1,2.00,2.00),
       (102,3,22,1,9.00,9.00),
       (103,3,33,1,15.00,15.00),
       (104,3,61,2,1.00,2.00),
       
       (201,6,21,1,8.00,8.00),
       (202,6,31,1,12.00,12.00),
       (203,6,62,1,1.00,1.00),
       
       (301,19,12,1,2.50,2.50),
       (302,19,23,1,9.00,9.00),
       (303,19,41,1,6.00,6.00),
       (304,19,61,1,1.00,1.00);
        ");

        if (!$exeResult) {
            print($pdo->errorInfo());
        }
    }

    final public function fillProducts()
    {
        $pdo = $this->getConnection();
        $exeResult = $pdo->exec("
INSERT INTO `productCategories` (`id`, `order`, `name`) 
VALUES
       (1,1,'Antipasti')
       ,(2,2,'Primi')
       ,(3,3,'Secondi')
       ,(4,4,'Contorni')
       ,(5,5,'Dolci')
       ,(6,6,'Bibite');       
        ");

        if (!$exeResult) {
            print($pdo->errorInfo());
        }

        $exeResult = $pdo->exec("
INSERT INTO `products` (`id`, `categoryId`, `productName`, `price`, `quantity`)
VALUES
       (11,1,'Arancini di riso',2.00,3)
       ,(12,1,'Mozzarella in carrozza',2.50,2)
       ,(21,2,'Spaghetti alla carbonara',8.00,5)
       ,(22,2,'Lasagne alla bolognese',9.00,5)
       ,(23,2,'Trofie al pesto',9.00,4)
       ,(31,3,'Orata al forno',12.00,0)
       ,(32,3,'Polpette al forno',9.00,4)
       ,(33,3,'Frittura mista',15.00,2)
       ,(41,4,'Patate al forno',6.00,2)
       ,(42,4,'Verdure grigliate',5.00,3)
       ,(51,5,'Tiramisu',4.00,8)
       ,(52,5,'Crostata di fragole',4.00,6)
       ,(61,6,'Acqua naturale',1.00,18)
       ,(62,6,'Acqua gassata',1.00,18);
        ");

        if (!$exeResult) {
            print($pdo->errorInfo());
        }
    }

    final public function fillMenuItems()
    {
        $pdo = $this->getConnection();
        $exeResult = $pdo->exec("
        INSERT INTO `menuItems` (`date`, `productId`) 
        VALUES
               ('2021-05-12', 11),
               ('2021-05-12', 21),
               ('2021-05-12', 31),
               ('2021-05-12', 41),               
               ('2021-05-13', 22),
               ('2021-05-13', 23),
               ('2021-05-13', 33)
        ");

        if (!$exeResult) {
            print($pdo->errorInfo());
        }
    }
}

?>