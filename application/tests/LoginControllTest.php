<?php

require_once("TestBase.php");

class LoginControllTest extends TestBase
{
    public function testGetUsersFromEmptyDatabase()
    {
        $model = new Users();
        $users = $model->selectAllUsers();
        $this->assertEquals(0, count($users), "Users table should be empty");
    }

    public function testWrongLogin()
    {
        $this->fillUsers();
        $model = new LoginControll();
        $loginResult = $model->login("abusatto", "123");
        $this->assertFalse($loginResult);
        $this->assertFalse($model->checkLogin());
        $this->assertFalse($model->session->userdata('login'));
        $this->assertNull($model->get_user('user'));
        $this->assertNull($model->session->userdata('administrator'));
    }

    public function testCorrectLogin()
    {
        $this->fillUsers();
        $model = new LoginControll();
        $loginResult = $model->login("abusatto", "123456");
        $this->assertTrue($loginResult);
        $this->assertTrue($model->checkLogin());
        $this->assertTrue($model->session->userdata('login'));
        $user = $model->get_user('user');
        $this->assertNotNull($user);
        $this->assertEquals(4, $user->id);
        $this->assertEquals("abusatto", $user->login);
        $this->assertNull($model->session->userdata('administrator'));

        $this->assertTrue($model->logout());
        $this->assertNull($model->session->userdata('login'));
        $this->assertNull($model->session->userdata('user'));
        $this->assertNull($model->session->userdata('administrator'));
    }

    public function testCorrectAdminLogin()
    {
        $this->fillUsers();
        $model = new LoginControll();
        $loginResult = $model->login("administrator", "adminpassword");
        $this->assertTrue($loginResult);
        $this->assertTrue($model->checkLogin());
        $this->assertTrue($model->session->userdata('login'));
        $user = $model->get_user('user');
        $this->assertNotNull($user);
        $this->assertEquals(1, $user->id);
        $this->assertEquals("administrator", $user->login);
        $this->assertTrue($model->session->userdata('administrator'));

        $this->assertTrue($model->logout());
        $this->assertNull($model->session->userdata('login'));
        $this->assertNull($model->session->userdata('user'));
        $this->assertNull($model->session->userdata('administrator'));
    }
}
