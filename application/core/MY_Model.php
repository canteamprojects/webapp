<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model{
	public $tableName="";
	public $idName="";

	public function delete(string $idVal){
		return $this->utility->deleteDb($this->tableName,$this->idName,$idVal);
	}

	public function insert(array $data){
      $this->validateData($data);
			return $this->utility->insertDb($this->tableName,$data);
	}

	public function update($table){
  	$this->validateData($data);
		return $this->utility->updateDb($this->tableName,$this->idName,$data);
	}

  public function validateData(&$data){
    //metodo per eventuali cast
  }

}
