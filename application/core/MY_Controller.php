<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

  protected $viewFolder="";
  protected $model="";

  /**
  * [__construct description]
  * @param boolean $checkLogin [description]
  */
  function __construct() {
    parent::__construct();
    $this->load->model('loginControll');
    $checkLogin=$this->loginControll->checkLogin();
    //Se il login non è andato a buon fine porta alla pagina login
    if (!$checkLogin) redirect('login/');
  }//end construct

  /**
  * Torna il model principale per la form
  * @return [type] [description]
  */
  public function getModel(){
    $model=$this->model;
    $this->load->model($model,"modelController");
    return $this->modelController;
  }

  public function doDelete($id = 0){
    //DELETE in MY_Model
    //TODO confirm delete
    $this->getModel()->delete($id);
    $this->index();
  }

  /**
  * Insert standard
  * @return [type] [description]
  */
  public function doInsert(){
    $data=$this->input->post();
    //INSERT in MY_Model
    $id=$this->getModel()->insert($data);
    $this->index($data);
  }

  /**
  * Update standard
  * @return [type] [description]
  */
  public function doUpdate(){
    $data=$this->input->post();
    //UPDATE in MY_Model
    $id=$this->getModel()->update($data);
    $this->index($data);
  }

}
