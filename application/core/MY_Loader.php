<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Loader extends CI_Loader {
    public function template($view_name, $vars = array(), $return = FALSE)
    {

      if($return):
        $content  = $this->view('template/htmlopen', $vars, $return);
        $content .= $this->view('template/header', $vars, $return);
        //$vars["menu"] = $this->Menu_model->htmlGenerate();
        if (isset($_SESSION['administrator'])) $content .= $this->view('template/menuAdministrator', $vars, $return);
        else $content .= $this->view('template/menu', $vars, $return);
        $content .= $this->view($view_name, $vars, $return);
        $content .= $this->view('template/footer', $vars, $return);
        $content .= $this->view('template/htmlclose', $vars, $return);
        return $content;
     else:

        $this->view('template/htmlopen', $vars);
        $this->view('template/header', $vars);
        //$vars["menu"] = $this->Menu_model->htmlGenerate();
        if (isset($_SESSION['administrator']))  $this->view('template/menuAdministrator', $vars, $return);
        else $this->view('template/menu', $vars, $return);
        $this->view('template/container', $vars);
        $this->view("template/errors",['errors'=>$this->utility->getErrors()]);
        $this->view("template/success",['success'=>$this->utility->getSuccess()]);
        $this->view($view_name, $vars);
        $this->view('template/footer', $vars);
        $this->view('template/htmlclose', $vars);
     endif;
    }
}

?>
