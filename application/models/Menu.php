<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Model{
    function __construct() {
       parent::__construct();
       $this->tableName="orders";
   		 $this->idName="id";
     }

   function addProduct($product){
     $this->utility->insertDb('products',$product);
   }

   function listAllProduct(){
     $result = $this->db->query("SELECT products.id as productId, productName,price, name, quantity
                                 FROM products
                                 JOIN productCategories ON products.categoryId = productCategories.id");
     return $result->result_array();
   }

   function removeProduct($productId){
     $this->utility->deleteDb('products','id',$productId);
   }

   function detailProduct($productId){
     $sql = "SELECT products.id as productId, productName,price, name, categoryId, quantity
             FROM products
             JOIN productCategories ON products.categoryId = productCategories.id
             WHERE products.id = ?";
     return $this->db->query($sql, array($productId))->result_array();
   }

   function categoryProductOption($id = 0){
     $sql = "SELECT *
             FROM productCategories";
     $categories = $this->db->query($sql)->result_array();
     $html = "";
     $selected = "";
     foreach ($categories as $value) {
       if ($id == $value["id"]) $selected ="selected";
       $html.= "<option $selected value={$value['id']}>{$value['name']}</option>";
       $selected = "";
     }
     return $html;
   }

   function productOption($date = ""){
     $sql = "SELECT *
             FROM products
             ORDER BY productName";
     $products = $this->db->query($sql)->result_array();
     $selectedProducts = array();
     if ($date){
       $selectedProducts = $this->listRoundProducts($date);
     }
     $html = "";
     $selected = "";
     foreach ($products as $value) {
       if (in_array($value["id"],$selectedProducts)) $selected ="selected";
       $html.= "<option $selected value={$value['id']}>{$value['productName']}</option>";
       $selected = "";
     }
     return $html;
   }

   function listRoundProducts($date){
     $return = array();
     $sql = "SELECT productId
             FROM menuItems
             WHERE date = ?";
     $res =  $this->db->query($sql,$date)->result_array();
     foreach ($res as $value) {
       array_push($return,$value['productId']);
     }
     return $return;
   }

   function getMenuForDate(string $date): iterable
   {
       $sql =  "SELECT p.id as id, p.productName as name, p.categoryId as category, price, quantity
                FROM products p
                INNER JOIN menuItems m ON m.productId = p.id AND m.date = ?
                WHERE quantity > 0";
       return $this->db->query($sql, $date)->result_array();
   }
}
