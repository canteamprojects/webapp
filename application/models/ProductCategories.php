<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ProductCategories extends MY_Model
{
    const Table = "productCategories";

    function __construct()
    {
        parent::__construct();
        $this->tableName = self::Table;
        $this->idName = "id";
    }

    public $id;
    public $order;
    public $name;

    public static function getAll(): iterable
    {
        $CI =& get_instance();
        return $CI->db->order_by('order', 'ASC')->get(self::Table)->result('ProductCategories');
    }

    public function toString(): string
    {
        return $this->name;
    }
}
