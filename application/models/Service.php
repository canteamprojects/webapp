<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends MY_Model{
    function __construct() {
       parent::__construct();
       $this->tableName="menuItems";
   		 $this->idName="id";
     }

   function add($data){
     $products = $data['products'];
     $this->utility->deleteDb($this->tableName,'date',$data['date']);
     foreach ($products as $product) {
       $this->utility->insertDb($this->tableName,array('productId' => $product, 'date' => $data['date']));
     }
   }

   function listAllService(){
     $result = $this->db->query("SELECT date,

                                        GROUP_CONCAT(productName SEPARATOR ', ') as products
                                 FROM menuItems
                                 LEFT JOIN products on products.id = menuItems.productId
                                 GROUP BY date");
     return $result->result_array();
   }

   function removeService($date){
     //remove related products
     $this->utility->deleteDb($this->tableName,'date',$date);
   }

   function detailService($serviceData){
     $sql = "SELECT date,
                    GROUP_CONCAT(productName SEPARATOR ', ') as products
             FROM menuItems
             LEFT JOIN products on products.id = menuItems.productId
             WHERE date = ?
             GROUP BY date";
     return $this->db->query($sql, array($serviceData))->result_array();
   }



}
