<?php
defined('BASEPATH') or exit('No direct script access allowed');

abstract class RoundsSlotsType
{
    const Pranzo = 'pranzo';
    const Cena = 'cena';
}

class RoundsSlots extends MY_Model
{
    const Table = "roundsSlots";

    function __construct()
    {
        parent::__construct();
        $this->tableName = self::Table;
        $this->idName = "id";
    }

    public $id;
    public $startTime;
    public $endTime;
    public $numberOfSlots;
    public $discount;
    public $type;

    public static function getAll(): iterable
    {
        $CI =& get_instance();
        return $CI->db->get(self::Table)->result('RoundsSlots');
    }

    public function toString(): string
    {
        return $this->startTime.' - '.$this->endTime;
    }
}
