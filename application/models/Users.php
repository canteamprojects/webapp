<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends MY_Model
{
    const Table = "users";

    function __construct()
    {
        parent::__construct();
        $this->tableName = self::Table;
        $this->idName = "id";
    }

    public $id;
    public $login;
    public $surname;
    public $name;
    public $isAdmin;
    public $indirizzo;
    public $telefono;
    public $email;
    public $immagine;

    public static function login($login, $password): ?Users
    {
        $CI =& get_instance();
        $sql = "SELECT * FROM users WHERE login = ? AND password = ?";
        $res = $CI->db->query($sql, [$login, $password])->result(self::Table);
        if(count($res) == 1)
            return $res[0];
        else
            return NULL;
    }

    public function selectAllUsers()
    {
        $stm = $this->db->query("
        SELECT id
             , login
             , name
             , isAdmin
             , surname
             , indirizzo
             , dataDiNascita
             , TO_BASE64(immagine) AS immagine
             , telefono
             , email
             , (SELECT COUNT(*) FROM orders WHERE orders.userId = u.id) as ord
             , (SELECT COUNT(*) FROM orders WHERE orders.userId = u.id AND (orders.state = 'Confirmed' OR orders.state = 'Paid')) AS ordConf
             , (SELECT COUNT(*) FROM orders WHERE orders.userId = u.id AND orders.state = 'Inserted') AS ordNonConf
        FROM users u
        WHERE isAdmin = 0");
        return $stm->result_array();
    }

    public function selectUser($userId)
    {
        $queryText = "
        SELECT login
             , surname
             , name
             , isAdmin
             , indirizzo ,telefono,dataDiNascita,email,immagine
             , TO_BASE64(u.immagine) AS immagineBase64
             , (SELECT COUNT(*) FROM orders o WHERE o.UserId = u.id AND o.state = 'Inserted') AS insertedOrders
             , (SELECT COUNT(*) FROM orders o WHERE o.UserId = u.id AND (o.state = 'Confirmed' OR o.state = 'Paid')) AS confirmedOrders
             , (SELECT COUNT(*) FROM orders o WHERE o.UserId = u.id AND o.state = 'PickedUp') AS pickedUpOrders
          FROM users u 
         WHERE u.id = ?";
        $stm = $this->db->query($queryText, array($userId));
        return $stm->row();
    }

    public function inserUser($user){
        $this->insert($user);
    }
}
