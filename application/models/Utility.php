<?php

class Utility extends CI_Model
{
  public $errorMess=[];
  public $dbError=[];

  public $successMess=[];
  public $logMess=[];

  public function ifDebug(){
    return DEBUG;
  }

  public function getSuccess(){
    $data = $this->session->userdata('successMess');
    $this->session->unset_userdata('successMess');
    return $data;
  }

  public function addSuccess($mess){
    $data = $this->session->userdata('successMess');
    if (is_array($data)) array_push($data,$mess);
    else $data[0] = $mess;
    $this->session->set_userdata('successMess',$data);
  }

  public function getErrors(){
    $data = $this->session->userdata('errorMess');
    $this->session->unset_userdata('errorMess');
    return $data;
  }

  public function addError($mess){
    $data = $this->session->userdata('errorMess');
    if (is_array($data)) array_push($data,$mess);
    else $data[0] = $mess;
    $this->session->set_userdata('errorMess',$data);
  }

  public function insertDb($tableName,$data){
    if (!empty($data)) {
    //disable codeIgniter db error debug
      $this->db->db_debug = FALSE;
      $this->db->insert($tableName,$data);
      $this->db->db_debug = TRUE;
      $error = $this->db->error();
      if(!empty($error['code'])){
        $this->addError("Errore nell'Inserimento");
        return false;
      }
      $this->addSuccess("Salvataggio eseguito con Successo");
      return $this->db->insert_id();
    }
    $this->addError("Nessun dato da Inserire");
    return false;
  }

  public function updateDb($tableName,$idName,$data){
    if (!empty($data[$idName])) {
      $this->db->where($idName, $data[$idName]);
      //disable codeIgniter db error debug
      $this->db->db_debug = FALSE;
      $this->db->update($tableName, $data);
      $this->db->db_debug = TRUE;
      $error = $this->db->error();
      if(!empty($error['code'])){
        $this->addError("Errore nell'Inserimento");
        return false;
      }
      $this->addSuccess("Salvataggio eseguito con Successo");
      return true;
    }
    $this->utility->addError("Nessun record da Aggiornare");
    return false;
  }

  public function deleteDb($tableName,$idField,$idVal){
    if (!empty($idVal)) {
      //TODO Verificare che l'elemento esiste
      $id = array($idField => $idVal);
      //disable codeIgniter db error debug
      $this->db->db_debug = FALSE;
      $this->db->delete($tableName, $id);
      $this->db->db_debug = TRUE;
      $error=$this->db->error();
      if(!empty($error['code'])){
        $this->addError("Errore nella Cancellazione");
        return false;
      }
      $this->addSuccess("Cancellazione Avvenuta con Successo");
      return true;
    }
    $this->utility->addError("Non è possibile cancellare un id vuoto");
    return false;

  }

}
