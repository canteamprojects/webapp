<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Orders extends MY_Model
{
    function __construct()
    {
        parent::__construct();
        $this->tableName = "orders";
        $this->idName = "id";
    }

    public $id;
    public $userId;
    public $total;
    public $state;
    public $paymentType;
    public $slotId;
    public $code;

    function getList()
    {


// Non so se le query sia meglio scriverle come segue o con il metodo chaining - è da considerare importante usare il query builder caching che ho osservato sul manuale di codeigneter?

        $this->db->select('u.name as nomeUtente, u.surname as cognomeUtente, u.indirizzo as indirizzo, u.telefono as telefono, u.email as email, o.id as ordine, o.paymentType as pagamento, o.total as totale, o.state as stato, GROUP_CONCAT(oi.quantity SEPARATOR ",") as quantita, GROUP_CONCAT(oi.unitaryPrice SEPARATOR ",") as prezzoSingolo, GROUP_CONCAT(p.productName SEPARATOR ",") as nomeProdotto, rs.discount as sconto, rs.startTime as slot, o.date as giorno');
        $this->db->from('orders as o, products as p');
        $this->db->join('users as u', 'u.id = o.userId');
        $this->db->join('orderItems as oi', 'oi.orderId = o.id');

// Penso manchi chiave esterna nella tabella Orders per puntare alla tabella RoundSlots - la ho chiamata slotId nella istruzione che segue (e ho aggiornato il database)

        $this->db->join('roundsSlots as rs', 'rs.id = o.slotId');
        $this->db->where('oi.productId = p.id');
        $this->db->group_by('ordine');

        $this->db->order_by('giorno', 'DESC');

        $query = $this->db->get();

        return $query->result_array();
    }

    // Riordina la lista secondo certi criteri

    function listByValue($vars)
    {
        $this->db->select('u.name as nomeUtente, u.surname as cognomeUtente, u.indirizzo as indirizzo, u.telefono as telefono, u.email as email, o.id as ordine, o.paymentType as pagamento, o.total as totale, o.state as stato, GROUP_CONCAT(oi.quantity SEPARATOR ",") as quantita, GROUP_CONCAT(oi.unitaryPrice SEPARATOR ",") as prezzoSingolo, GROUP_CONCAT(p.productName SEPARATOR ",") as nomeProdotto, rs.discount as sconto, rs.startTime as slot, o.date as giorno');
        $this->db->from('orders as o, products as p');
        $this->db->join('users as u', 'u.id = o.userId');
        $this->db->join('orderItems as oi', 'oi.orderId = o.id');

        $this->db->join('roundsSlots as rs', 'rs.id = o.slotId');
        $this->db->where('oi.productId = p.id');
        $this->db->group_by('ordine');

        $this->db->order_by($vars, 'ASC');
        $this->db->order_by('slot', 'DESC');

        $query = $this->db->get();

        return $query->result_array();
    }

// Estrapola dalla lista un ordine particolare

    function getElement($vars)
    {
        $queryText = "
    SELECT u.name AS nomeUtente, u.surname AS cognomeUtente, u.indirizzo AS indirizzo, u.telefono  AS telefono, u.email AS email, o.id AS ordine, o.paymentType AS pagamento, o.total AS totale, o.state AS stato, GROUP_CONCAT(oi.quantity SEPARATOR ',') AS quantita, GROUP_CONCAT(oi.unitaryPrice SEPARATOR ',') AS prezzoSingolo, GROUP_CONCAT(p.productName SEPARATOR ',') AS nomeProdotto, rs.discount AS sconto, rs.startTime AS slot, o.date AS giorno
      FROM orders o
      JOIN users u ON u.id = o.userId
      JOIN orderItems oi ON o.id = oi.orderId
      JOIN roundsSlots rs ON o.slotId = rs.id
      JOIN products p ON oi.productId = p.id
      WHERE o.id = ?
      GROUP BY o.id";
        $stm = $this->db->query($queryText, array($vars));
        return $stm->result_array();
    }

    function getUserOrders($userId)
    {
        $queryText = "
    SELECT o.id
	     , o.total
         , o.state
         , o.paymentType
         , o.date as orderDate
         , o.code
         , s.startTime
         , s.endTime
         , GROUP_CONCAT(productName SEPARATOR ', ') AS details
         , s.discount
      FROM orders o
      JOIN roundsSlots s ON o.slotId = s.id
      JOIN orderItems oi ON o.id = oi.orderId
	  JOIN products p ON oi.productId = p.id
     WHERE o.userId = ?
     GROUP BY o.id
     ORDER BY o.id DESC";
        $stm = $this->db->query($queryText, array($userId));
        return $stm->result_array();
    }

    function getOrder($orderId)
    {
        $queryText = "
    SELECT *
      FROM orders o
      JOIN roundsSlots rs ON rs.id = o.slotId
      JOIN users u ON u.id = o.userId
     WHERE o.id = ?";
        $stm = $this->db->query($queryText, array($orderId));
        return $stm->row();
    }

    function getOrderItems($orderId)
    {
        $queryText = "
    SELECT i.*, p.productName
      FROM orders o
      JOIN orderItems i ON i.orderId = o.id
      JOIN products p ON i.productId = p.id
     WHERE o.id = ?";
        $stm = $this->db->query($queryText, array($orderId));
        return $stm->result_array();
    }

    function pickupOrder($orderId)
    {
        $data = array(
            'State' => 'PickedUp'
        );
        $where = "(State = 'Confirmed' OR State = 'Paid') AND id = " . $orderId;
        $this->db->update('orders', $data, $where);
    }

    function insertOrder($order)
    {
        $code = $this->getFreeCode($order->date);
        $data = array(
            'userId' => $order->userId,
            'total' => $order->totalWithDiscount,
            'state' => 'Inserted',
            'paymentType' => null,
            'slotId' => $order->slot,
            'date' => $order->date,
            'code' => $code
        );
        $this->db->insert($this->tableName, $data);
        $orderId = $this->db->insert_id();

        foreach ($order->items as $item) {
            $data = array(
                'orderId' => $orderId,
                'productId' => $item->id,
                'quantity' => $item->qty,
                'unitaryPrice' => $item->price,
                'total' => $item->total,
            );
            $this->db->insert('orderItems', $data);
        }

        return ['id' => $orderId, 'code' => $code];
    }

    function setOrderPaidWithCreditCard($orderId, $owner, $ccn, $exp, $cvc)
    {
        $data = array(
            'orderId' => $orderId,
            'cardType' => 'Credit',
            'cardOwner' => $owner,
            'cardNumber' => $ccn,
            'cardExpirationDate' => $exp,
            'cardCVC' => $cvc,
        );
        $this->db->insert('cardPayments', $data);

        $orderUpdateData = array(
            'State' => 'Paid',
            'PaymentType' => 'CreditDebitCard'
        );
        $where = "id = " . $orderId;
        $this->db->update('orders', $orderUpdateData, $where);
    }

    function setOrderPaidWithCash($orderId)
    {
        $orderUpdateData = array(
            'State' => 'Paid',
            'PaymentType' => 'Cash'
        );
        $where = "id = " . $orderId;
        $this->db->update('orders', $orderUpdateData, $where);
    }

    function getFreeCode($date)
    {
        $sql = "SELECT code
                FROM orders
                WHERE date = ?";
        $result = $this->db->query($sql, array($date))->result_array();
        $usedCode = array_map(function ($i) {
            return $i['code'];
        }, $result);

        $code = rand(0, 9999);
        while (in_array($code, $usedCode)) {
            $code = ($code + 1) % 10000;
        }
        return $code;
    }

    function getOrderByCode($code, $data)
    {
        $queryText = "
            SELECT o.*, rs.startTime, rs.endTime, u.name, u.surname
            FROM orders o
            JOIN roundsSlots rs ON rs.id = o.slotId
            JOIN users u ON u.id = o.userId
            WHERE o.code = ?
            AND o.date = ?";
        $stm = $this->db->query($queryText, array($code, $data));
        return $stm->result_array()[0];
    }
}
