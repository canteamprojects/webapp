<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LoginControll extends CI_Model
{

    public function checkLogin()
    {
        return ($this->session->has_userdata('login') && $this->session->login);
    }

    public function login($user, $password)
    {
        $user = $this->users->login($user, $password);

        $loginCheck = $user != NULL;
        $this->session->set_userdata('login', $loginCheck);

        if ($loginCheck) {
            $this->session->set_userdata('user', serialize($user));
            if ($user->isAdmin)
                $this->session->set_userdata('administrator', TRUE);
        }
        return $loginCheck;
    }

    /**
     * Get the current user from session
     * @return Users|null
     */
    public function get_user(): ?Users
    {
        $user = $this->session->userdata('user');
        return isset($user) ? unserialize($user) : null;
    }

    public function logout(): bool
    {
        $this->session->unset_userdata('login');
        $this->session->unset_userdata('user');
        $this->session->unset_userdata('administrator');
        return true;
    }
}
