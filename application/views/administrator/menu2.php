<html lang="it">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Comandi Amministratore</title>
    <link href="vendor/almasaeed2010/adminlte/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link rel="stylesheet" href="vendor/almasaeed2010/adminlte/dist/css/amministratore.css" type="text/css" media="all"/>
    <link rel="stylesheet" href="vendor/almasaeed2010/adminlte/dist/css/pannellosinistro.css" type="text/css" media="all"/>
</head>

<body>

    <input type="checkbox" id="nav-toggle" hidden="">

    <nav class="nav">

        <label for="nav-toggle" class="nav-toggle" onclick=""></label>

        <div class="img_logo">
            <img src="vendor/almasaeed2010/adminlte/dist/img/logo.jpg" alt="Logo" title="Logo"></a>
        </div>

        <ul>

            <li><a href="#">Inserisci disponibilità magazzino </a></li>

            <li><a href="menu.php">Inserisci impostazioni giornaliere</a></li>

            <li><a href="#">Modifica impostazioni giornaliere</a></li>

        </ul>
    </nav>

<!-- variabile PHP per ogni campo, che genera un messaggio di errore se combinata con la seguente estensione del codice
  controllo che si può lasciare nella view CONTROLLO TESTATO E FUNZIONA -->

 <?php

if ( !empty($_POST)) {
// Rilevare un errore alla conferma
$nomeError = null;
$prezzoError = null;
$descrizioneError = null;
$startError = null;
$endError = null;
$disocuntError = null;
$prezzomenuError = null;

// Rilevare i valori inseriti
$nome = $_POST['nome'];
$prezzo = $_POST['prezzo'];
//$immagine = $_POST['immagine'];
$descrizione = $_POST['descrizione'];
$start = $_POST['start'];
$end = $_POST['end'];
$discount = $_POST['discount'];
$prezzomenu = $_POST['prezzomenu'];

// Confermare l’inserimento
$valid = true;

if (empty($nome)) {
$nomeError = 'Inserire un nome';
$valid = false; } else if (!preg_match('/^[A-Za-z \'-]+$/i',$nome)) {
$nomeError = 'Il nome contiene caratteri non ammessi';
$valid = false;
}

if (empty($descrizione)) {
$descrizioneError = 'Inserire una descrizione';
$valid = false;
} else if ( strlen($descrizione)>200 ) {
$descrizioneError = 'Inserire una descrizione più breve';
$valid = false;
}

if (empty($prezzo)) {
$prezzoError = 'Inserire un prezzo';
$valid = false;
} else if ( ($prezzo)<0.0 ) {
$prezzoError = 'Inserire un prezzo positivo';
$valid = false;
} else if ( !filter_var($prezzo,FILTER_VALIDATE_FLOAT) ) {
$prezzoError = 'Inserire un prezzo con la virgola';
$valid = false;
}

if (empty($discount)) {
$discountError = 'Inserire uno sconto';
$valid = false;
}

if (empty($start)) {
$startError = 'Inserire un orario di inizio';
$valid = false;
}

if (empty($end)) {
$endError = 'Inserire un orario di fine';
$valid = false;
} else if ( ($end) < ($start) ) {
$endError = 'Orario di fine impossibile';
$valid = false;
}

if (empty($prezzomenu)) {
$prezzomenuError = 'Inserire un prezzo';
$valid = false;
} else if ( ($prezzomenu)<0.0 ) {
$prezzomenuError = 'Inserire un prezzo positivo';
$valid = false;
} else if ( !filter_var($prezzomenu,FILTER_VALIDATE_FLOAT) ) {
$prezzomenuError = 'Inserire un prezzo con la virgola';
$valid = false;
}

// Inserire i dati si chiama il model
if ($valid) {

    // $sql = "INSERT INTO

}
 }
?>

<!-- fine dei contolli sui campi inseriti dal utente -->

    <div class="container-fluid impaginazione">

        <div class="row">



        <div class="col-sm-4">

            <h2>Inserisci i prodotti </h2>

            <form class="form-horizontal" action="#" method="post">

                <div class="form-group <?php echo !empty($nomeError)?'has-error':'';?>">
                   <label class="control-label">Nome</label>
                   <div class="controls">
                       <input name="nome" type="text" placeholder="Nome" value="<?php echo !empty($nome)?$nome:'';?>">
                       <?php if (!empty($nomeError)): ?>
                            <span class="help-inline"><?php echo   $nomeError;?></span>
                       <?php endif; ?>
                   </div>
                </div>



                <div class="form-group <?php echo !empty($prezzoError)?'has-error':'';?>">
                    <label class="control-label">Prezzo</label>
                    <div class="controls">
                       <input name="prezzo" type="number" step="0.01" placeholder="Prezzo" value="<?php echo !empty($prezzo)?$prezzo:'';?>">
                       <?php if (!empty($prezzoError)): ?>
                            <span class="help-inline"><?php echo   $prezzoError;?></span>
                       <?php endif;?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label">Immagine</label>
                    <div class="controls">
                       <select id="immagine">

                           <!-- questa view sarà lanciata da un controler che carica ed esegue un model che interroga ed estrapola tutte le foto presenti in archivio
                             quindi, anzichè una lista di option ci sarà un foreach con interrogazione tabella products e select campo varchar immagine che non c'è
                           attualmente nel database (non c'è bisogno di nessun controllo perchè estrapola nomi esistenti cui scegliere senza sbagliare)
                           invece di valore uno o valore due istruzioni php echo e variabile restituita da model
                           -->

                            <option value="scelta uno">valore uno </option>
                            <option value="scelta due">valore due</option>
                       </select>
                    </div>
                </div>

                <div class="form-group <?php echo !empty($descrizioneError)?'has-error':'';?>">
                    <label class="control-label">Breve descrizione</label>
                    <div class="controls">
                        <input name="descrizione" type="text" placeholder="Breve descrizione" size="30" maxlength="200" value="<?php echo !empty($descrizione)?$descrizione:'';?>">
                        <?php if (!empty($descrizioneError)): ?>
                              <span class="help-inline"><?php echo $descrizioneError;?></span>
                        <?php endif;?>
                    </div>
                </div>


                <div class="form-actions">
                    <a class="btn btn-danger" data-toggle="popover" data-trigger="focus" title="Clicca qui per annullare richiesta di inserimento" href="application/views/administrator/menu.php">Annulla</a>
                    <button type="submit" class="btn btn-success" data-toggle="popover" data-trigger="focus" title="Clicca qui per confermare inserimento">Inserisci</button>
                </div>

            </form>

        </div>

          <div class="col-sm-4">

            <h2>Inserisci Timeslots e sconti </h2>

            <form class="form-horizontal" action="#" method="post">

                <div class="form-group <?php echo !empty($startError)?'has-error':'';?>">
                   <label class="control-label">Start Time</label>
                   <div class="controls">
                       <input name="start" type="time" min="12:00" value="<?php echo !empty($start)?$start:'';?>">
                       <?php if (!empty($startError)): ?>
                            <span class="help-inline"><?php echo   $startError;?></span>
                       <?php endif; ?>
                   </div>
                </div>


                <div class="form-group <?php echo !empty($endError)?'has-error':'';?>">
                   <label class="control-label">End Time</label>
                   <div class="controls">
                       <input name="end" type="time" max="22:00" value="<?php echo !empty($end)?$end:'';?>">
                       <?php if (!empty($endError)): ?>
                            <span class="help-inline"><?php echo   $endError;?></span>
                       <?php endif; ?>
                   </div>
                </div>

                <div class="form-group <?php echo !empty($discountError)?'has-error':'';?>">
                    <label class="control-label">Discount</label>
                    <div class="controls">
                       <input name="discount" type="number" min="0" max="30" placeholder="Discount" value="<?php echo !empty($discount)?$discount:'';?>">
                       <?php if (!empty($discountError)): ?>
                            <span class="help-inline"><?php echo   $discountError;?></span>
                       <?php endif;?>
                    </div>
                </div>


                <div class="form-actions">
                    <a class="btn btn-danger" data-toggle="popover" data-trigger="focus" title="Clicca qui per annullare richiesta di inserimento" href="application/views/administrator/menu.php">Annulla</a>
                    <button type="submit" class="btn btn-success" data-toggle="popover" data-trigger="focus" title="Clicca qui per confermare inserimento">Inserisci</button>
                </div>

            </form>

        </div>

         <div class="col-sm-4">

            <h2>Inserisci i menu </h2>

            <form class="form-horizontal" action="#" method="post">

                 <div class="form-group">
                    <label class="control-label">Primo</label>
                    <div class="controls">
                       <select id="primo">

                           <!-- questa view sarà lanciata da un controler che carica ed esegue un model che interroga ed estrapola tutti primi piatti presenti in archivio
                             quindi, anzichè una lista di option ci sarà un foreach con interrogazione giunzione tra tabelle products e categorieprodotti e select
                             lista di scelte di primi
                             invece di valore uno o valore due istruzioni php echo e variabile restituita da model
                            -->

                            <option value="scelta uno">valore uno </option>
                            <option value="scelta due">valore due</option>
                       </select>
                    </div>
                 </div>


                 <div class="form-group">
                    <label class="control-label">Secondo</label>
                    <div class="controls">
                       <select id="secondo">

                          <!-- questa view sarà lanciata da un controler che carica ed esegue un model che interroga ed estrapola tutti i secondi piatti presenti in archivio
                             quindi, anzichè una lista di option ci sarà un foreach con interrogazione giunzione tra tabelle products e categorieprodotti e select
                             lista di scelte di secondi
                             invece di valore uno o valore due istruzioni php echo e variabile restituita da model
                            -->


                            <option value="scelta uno">valore uno </option>
                            <option value="scelta due">valore due</option>
                       </select>
                    </div>
                 </div>


                 <div class="form-group">
                    <label class="control-label">Contorni</label>
                    <div class="controls">
                       <select id="contorno">

                          <!-- questa view sarà lanciata da un controler che carica ed esegue un model che interroga ed estrapola tutti i contorni presenti in archivio
                             quindi, anzichè una lista di option ci sarà un foreach con interrogazione giunzione tra tabelle products e categorieprodotti e select
                             lista di contorni
                             invece di valore uno o valore due istruzioni php echo e variabile restituita da model
                            -->


                            <option value="scelta uno">valore uno </option>
                            <option value="scelta due">valore due</option>
                       </select>
                    </div>
                 </div>

                 <div class="form-group">
                    <label class="control-label">Dolci</label>
                    <div class="controls">
                       <select id="dolce">

                          <!-- questa view sarà lanciata da un controler che carica ed esegue un model che interroga ed estrapola tutti i dolci presenti in archivio
                             quindi, anzichè una lista di option ci sarà un foreach con interrogazione giunzione tra tabelle products e categorieprodotti e select
                             lista di scelte di dolci
                             invece di valore uno o valore due istruzioni php echo e variabile restituita da model
                            -->


                            <option value="scelta uno">valore uno </option>
                            <option value="scelta due">valore due</option>
                       </select>
                    </div>
                 </div>

                <div class="form-group <?php echo !empty($prezzomenuError)?'has-error':'';?>">
                    <label class="control-label">Prezzo</label>
                    <div class="controls">
                       <input name="prezzomenu" type="number" step="0.01" placeholder="Prezzo" value="<?php echo !empty($prezzomenu)?$prezzomenu:'';?>">
                       <?php if (!empty($prezzomenuError)): ?>
                            <span class="help-inline"><?php echo   $prezzomenuError;?></span>
                       <?php endif;?>
                    </div>
                </div>




                <div class="form-actions">
                    <a class="btn btn-danger" data-toggle="popover" data-trigger="focus" title="Clicca qui per annullare richiesta di inserimento" href="application/views/administrator/menu.php">Annulla</a>
                    <button type="submit" class="btn btn-success" data-toggle="popover" data-trigger="focus" title="Clicca qui per confermare inserimento">Inserisci</button>
                </div>

            </form>

        </div>


</div>


</div> <!-- /container -->







</body></html>
