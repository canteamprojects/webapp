<section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title text-white">Piatti</h3>
          <button type="button"  style="float: right;" class="product-insert-button btn btn-info btn-sm" data-toggle="modal" data-target="#modal-products">Inserisci Nuovo</button>

        </div>
        <div class="card-body p-1">
          <table class="table table-striped dataTable2 dtr-inline" role="grid" aria-describedby="example2_info">
              <thead>
              <tr>
                  <th style="width: 2%">
                      #
                  </th>
                  <th style="width: 15%">
                      Categoria
                  </th>
                  <th style="width: 43%">
                      Nome
                  </th>
                  <th style="width: 10%" class="text-center">
                      Prezzo
                  </th>
                  <th style="width: 10%">
                      Quantità
                  </th>
                  <th style="width: 20%">
                  </th>
              </tr>
              </thead>
              <tbody>
              <?php foreach ($products as $row) : ?>
              <tr>
                  <td>
                      <?= $row["productId"] ?>
                  </td>
                  <td>
                    <?= $row["name"] ?>
                  </td>
                  <td>
                    <?= $row["productName"] ?>
                  </td>
                  <td style = "text-align: center;">
                    <?= $row["price"] ?>
                  </td>
                  <td>
                    <?= $row["quantity"] ?>
                  </td>
                  <td class="project-actions text-right">


                      <button type="button" data-id="<?= $row["productId"] ?>" class="product-button btn btn-info btn-sm" data-toggle="modal" data-target="#modal-products">
                        Modifica
                      </button>
                      <button type="button" data-id="<?= $row["productId"] ?>" class="product-delete-button btn btn-danger btn-sm">
                        <i class="fas fa-trash">
                        </i>
                        Elimina
                      </button>
                  </td>
              </tr>
            <?php endforeach; ?>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
            <h3 class="card-title text-white">Servizi</h3>
          <button type="button"  style="float: right;" class="mb-0 service-insert-button btn btn-info btn-sm" data-toggle="modal" data-target="#modal-services">Inserisci Nuovo</button>
        </div>
        <div class="card-body p-1">
          <table class="table table-striped dataTable2 dtr-inline" role="grid" aria-describedby="example2_info">
              <thead>
              <tr>
                  <th style="width: 10%">
                      Data
                  </th>
                  <th style="width: 350px">
                      Piatti
                  </th>
                  <th style="width: 30%">
                  </th>
              </tr>
              </thead>
              <tbody>
              <?php foreach ($services as $row) : ?>
              <tr>

                  <td>
                      <a>
                          <?= $row["date"] ?>
                      </a>

                  </td>
                  <td >
                    <?= $row["products"] ?>
                  </td>
                  <td class="project-actions text-right">

                    <button type="button" data-id="<?= $row["date"] ?>" class="service-button btn btn-info btn-sm" data-toggle="modal" data-target="#modal-services">
                      Modifica
                    </button>
                    <button type="button" data-id="<?= $row["date"] ?>"  class="service-delete-button btn btn-danger btn-sm">
                      <i class="fas fa-trash">
                      </i>
                      Elimina
                    </button>
                  </td>
              </tr>
            <?php endforeach;?>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>

    <div class="modal fade" id="modal-products">
       <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
             <h4 class="modal-title">Piatto del menù</h4>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
             </button>
           </div>
           <form action="<?php echo base_url()?>index.php/modal/insertProduct" method="post">
           <div class="modal-body">

           </div>
           <div class="modal-footer justify-content-between">
             <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
             <input type="submit" class="btn btn-default" value="Salva">
           </div>
          </form>
         </div>
         <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
     </div>
     <!-- /.modal -->
     <div class="modal fade" id="modal-services">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Servizio</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="<?php echo base_url()?>index.php/modal/insertService" method="post">
            <div class="modal-body">

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
              <input type="submit" class="btn btn-default" value="Salva">
            </div>
           </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
