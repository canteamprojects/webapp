<?php
require_once(APPPATH . '/helpers/usersHelper.php');

use helpers\usersHelper;

?>

<section class="content">
    <!-- Default box -->
    <div class="card card-solid">
        <div class="card-body pb-0">
            <div class="row  align-items-stretch">
                <?php foreach ($result as $row) : ?>
                    <div class="col-6 col-md-4">
                        <div class="card bg-light">
                            <div class="card-header text-white border-bottom-0">
                                <?= $row['login'] ?>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-7">
                                        <h2 class="lead"><b><?= $row['name']; ?></b></h2>
                                        <h2 class="lead"><b><?= $row['surname']; ?></b></h2>
                                        <p class="text-muted text-sm"><?= $row['dataDiNascita'] ?></p>
                                        <p class="text-muted text-sm"><b>Informazioni: </b><br>Ordini
                                            effettuati: <?= $row['ord'] ?><br>Ordini ritirati: <?= $row['ordConf'] ?>
                                            <br>Ordini da ritirare:<?= $row['ordNonConf'] ?></p>
                                        <ul class="ml-4 mb-0 fa-ul text-muted">
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span>
                                                Indirizzo: <?= $row['indirizzo'] ?> </li>
                                            <li class="small"><span class="fa-li"><i
                                                            class="fas fa-lg fa-phone"></i></span> Numero di
                                                telefono:<?= $row['telefono'] ?></li>
                                        </ul>
                                    </div>

                                    <div class="col-5 text-center">
                                        <?php
                                        echo usersHelper::userPictureOrEmpty($row['immagine']);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="text-right">
                                    <a href="mailto:<?= $row['email'] ?>" class="btn btn-sm bg-teal">
                                        <i class="fas fa-comments"></i>
                                    </a>
                                    <a class="btn btn-sm btn-primary" href="<?php echo base_url(); ?>index.php/dashboard/profile/<?php echo $row['id']; ?>">
                                        <i class="fas fa-user"></i> Vedi Profilo
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              
            </div>
            <!-- /.card-footer -->
        </div>
        <!-- /.card -->
</section>
