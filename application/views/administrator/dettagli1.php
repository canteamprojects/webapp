<?php declare(strict_types=1) ?>
<section class="content">
    
    
      <div id="tabella" class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fas fa-globe"></i> Canteen Booking
                    <small class="float-right"><?php echo "Data : "; echo $datiOrdine['giorno'] ?></small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
          
                <div class="col-sm-6 invoice-col">
                  Cliente:
                  <address>
                    <strong> <?php echo $datiOrdine['nome']; echo " "; echo $datiOrdine['cognome'] ?></strong><br>
                    Indirizzo: <br>
                    <?php
                    $pos = strpos($datiOrdine['indirizzo'], ",");
                    $parte1 = substr($datiOrdine['indirizzo'], 0, $pos);
                    $parte2 = substr($datiOrdine['indirizzo'], $pos);
                    $str = substr($parte2, 1);
                    echo $parte1; ?> <br>
                    <?php echo $str; ?><br>
                    Telefono: <?php echo " "; echo $datiOrdine['telefono'];?><br>
                    Email: <?php echo " "; echo $datiOrdine['email'];?>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-6 invoice-col">
                  <b> <?php echo "N. Ordine #"; echo $datiOrdine['numOrdine'] ?></b><br>
                  <br>
                  <b>Stato dell'ordine:</b><?php echo " "; echo $datiOrdine['stato'] ?> <br>
                  <br>
                  <b>Tipo di pagamento:</b><?php echo " "; echo $datiOrdine['pagamento'] ?> <br>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>Quantit&agrave;</th>
                      <th>Prodotti</th>
                      <th>Descrizione</th>
                      <th>Prezzo Unitario</th>
                    </tr>
                    </thead>
                    <tbody>
                        
                    <?php 
                    $listaProdotti = $datiOrdine['listaProdotti'];
                    $elencoProdotti = $listaProdotti['prodotti'];
                    $stringaProdotti = implode(',', $elencoProdotti);
                    $elementoProdotto = explode (',' , $stringaProdotti);
                  
                    
                    $listaPrezzi = $datiOrdine['costo'];
                    $elencoPrezzi = $listaPrezzi['costo'];
                    $stringaPrezzi = implode(',', $elencoPrezzi);
                    $elementoPrezzo = explode (',' , $stringaPrezzi);
                    
                   
                    $listaQuantita = $datiOrdine['quantita'];
                    $elencoQuantita = $listaQuantita['quantita'];
                    $stringaQuantita = implode(',', $elencoQuantita);
                    $elementoQuantita = explode (',' , $stringaQuantita);
                    
                    $elementi = array('prodotti'=>$elementoProdotto, 'prezzi'=>$elementoPrezzo, 'quantita'=>$elementoQuantita);
                    
                    ?>
                  
                    <?php $index=0; $totaleParziale= 0; while($index < count ($elementi['prodotti'])){ ?>    
                    <tr>
                      <td><?php echo($elementi['quantita'][$index]); ?></td>
                      <td><?php echo($elementi['prodotti'][$index]); ?></td>
                      <td>Per la descrizione si dovrebbe aggiungere il campo in database e la modalità di inserimento lato amministratore</td>
                      <td><?php echo($elementi['prezzi'][$index]);?>&euro;</td>
                    </tr>
                    <?php $totaleParziale = ($totaleParziale+($elementi['prezzi'][$index])); $index++;  } ?>
                    
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-6">
                

                </div>
                <!-- /.col -->
                <div class="col-6">
                  <p class="lead">Importo</p>

                  <div class="table-responsive">
                    <table class="table">
                      <tbody><tr>
                        <th style="width:50%">Totale parziale:</th>
                        <td><?php echo number_format( $totaleParziale, 2); ?>&euro;</td>
                      </tr>
                      <tr>
                        <th>Time slot:</th>
                        <td><?php echo $datiOrdine['slot'] ?></td>
                      </tr>
                      <tr>
                        <th>Sconto:</th>
                        <td><?php echo $datiOrdine['sconto'] ?>&#37;</td>
                      </tr>
                      <tr>
                        <th>Totale:</th>
                        <td><?php echo $datiOrdine['totale'] ?>&euro;</td>
                      </tr>
                    </tbody></table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
               <div class="col-sm-6 invoice-col">
                  <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Stampa</a>
               </div>
                <div class="col-sm-6 invoice-col">
                  <a class="btn btn-primary btn-sm" style="float:right" href="#" >
                  <i class="fas fa-download">
                  </i> Generare PDF </a>
                </div>
               
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
