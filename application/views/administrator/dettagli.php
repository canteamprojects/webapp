<?php declare(strict_types=1) ?>
<section class="content">


    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Main content -->
                <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-12">
                            <h4>
                                <i class="fas fa-globe"></i> Canteen Booking
                                <small class="float-right"><?php echo "Data : ";
                                    echo $datiOrdine['giorno'] ?></small>
                            </h4>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">

                        <div class="col-sm-6 invoice-col">
                            Cliente:
                            <address>
                                <strong> <?php echo $datiOrdine['nome'];
                                    echo " ";
                                    echo $datiOrdine['cognome'] ?></strong><br>
                                795 Folsom Ave, Suite 600<br>
                                San Francisco, CA 94107<br>
                                Telefono: (555) 539-1037<br>
                                Email: john.doe@example.com
                            </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            <b> <?php echo "N. Ordine #";
                                echo $datiOrdine['numOrdine'] ?></b><br>
                            <br>
                            <b>Stato dell'ordine:</b><?php echo " ";
                            echo $datiOrdine['stato'] ?> <br>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Quantit�</th>
                                    <th>Prodotti</th>
                                    <th>Serial #</th>
                                    <th>Descrizione</th>
                                    <th>Prezzo Unitario</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                $listaProdotti = $datiOrdine['listaProdotti'];
                                $elencoProdotti = $listaProdotti['prodotti'];
                                $stringaProdotti = implode(',', $elencoProdotti);
                                $elementoProdotto = explode(',', $stringaProdotti);


                                $listaPrezzi = $datiOrdine['costo'];
                                $elencoPrezzi = $listaPrezzi['costo'];
                                $stringaPrezzi = implode(',', $elencoPrezzi);
                                $elementoPrezzo = explode(',', $stringaPrezzi);


                                $listaQuantita = $datiOrdine['quantita'];
                                $elencoQuantita = $listaQuantita['quantita'];
                                $stringaQuantita = implode(',', $elencoQuantita);
                                $elementoQuantita = explode(',', $stringaQuantita);

                                $elementi = array('prodotti' => $elementoProdotto, 'prezzi' => $elementoPrezzo, 'quantita' => $elementoQuantita);

                                ?>

                                <?php $index = 0;
                                while ($index < count($elementi['prodotti'])) { ?>
                                    <tr>
                                        <td><?php echo($elementi['quantita'][$index]); ?></td>
                                        <td><?php echo($elementi['prodotti'][$index]); ?></td>
                                        <td>455-981-221</td>
                                        <td>Articolo 1</td>
                                        <td><?php echo($elementi['prezzi'][$index]); ?>&euro;</td>
                                    </tr>
                                    <?php $index++;
                                } ?>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-6">


                        </div>
                        <!-- /.col -->
                        <div class="col-6">
                            <p class="lead">Importo</p>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th style="width:50%">Totale parziale:</th>
                                        <td><?php echo $datiOrdine['totale'] ?>&euro;</td>
                                    </tr>
                                    <tr>
                                        <th>Time slot:</th>
                                        <td><?php echo $datiOrdine['slot'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Sconto:</th>
                                        <td><?php echo $datiOrdine['sconto'] ?>&#37;</td>
                                    </tr>
                                    <tr>
                                        <th>Totale:</th>
                                        <td><?php $diff = (($datiOrdine['sconto'] * $datiOrdine['totale']) / 100);
                                            echo number_format($datiOrdine['totale'] - $diff, 2) ?>&euro;
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- this row will not appear when printing -->
                    <div class="row no-print">
                        <div class="col-12">
                            <a href="invoice-print.html" target="_blank" class="btn btn-default"><i
                                        class="fas fa-print"></i> Stampa</a>

                            <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                                <i class="fas fa-download"></i> Generare PDF
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.invoice -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
