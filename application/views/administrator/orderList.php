<?php
require_once(APPPATH . '/helpers/ordersHelper.php');

use helpers\ordersHelper;

?>
<section class="content">


    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title text-white">Ordini</h3>
            <!--
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
            </div> -->
        </div>

        <div class="card-body p-1">
            <table>
                <thead>
                <tr>
                    <th style="width: 30%">
                        <form method="post" accept-charset="utf-8"
                              action="<?php echo base_url(); ?>index.php/dashboard/orderElementAdmin">
                            <!--
                            <label for="ordine"></label>
                            <div style="float:left">
                                <input class="form-control" name="ordine" min="1" type="number"
                                       placeholder="Ricerca per ordine">
                            </div>
                            <input type="submit" class="btn btn-default" value="Ricerca"> -->
                        </form>
                    </th>
                    <th style="width: 50%">
                    </th>
                    <th style="width: 30%">
                        <form method="post" accept-charset="utf-8"
                              action="<?php echo base_url(); ?>index.php/dashboard/orderValueListAdmin">
                          <!--  <div style="float:left">
                                <select name="elenco" class="form-select, btn btn-outline-secondary">
                                    <option value="giorno" , selected>Ordina per giorno</option>
                                    <option value="pagamento">Ordina per tipo di pagamento</option>
                                    <option value="stato">Ordina per stato</option>
                                </select>
                            </div>
                            <input type="submit" class="btn btn-default" value="Ordina">
                          -->
                        </form>
                    </th>
                </tr>
                </thead>
            </table>

            <table class="table dataTable2 table-striped projects">

                <thead>


                <tr>
                    <th style="width: 1%">
                        #
                    </th>
                    <th style="width: 10%">
                        Ordine
                    </th>
                    <th style="width: 30%">
                        Dettagli
                    </th>
                    <th style="width: 10%" class="text-center">
                        Stato
                    </th>
                    <th style="width: 10%" class="text-center">
                        Tipo di pagamento
                    </th>
                    <th style="width: 20%">
                    </th>
                </tr>
                </thead>
                <tbody>


                <?php foreach ($databaseData as $row): ?>
                    <tr>
                        <td>
                            <?php echo $row['ordine'] ?>
                        </td>
                        <td>
                            <small>
                                Data <?php echo " ";
                                echo $row['giorno'] ?> <br>
                                Slot <?php echo " ";
                                echo $row['slot'] ?>
                            </small>
                        </td>
                        <td>
                            <a>
                                <?php echo $row['nomeProdotto'] ?>
                            </a>
                        </td>
                        <td class="project-state">
                            <?php
                            echo ordersHelper::orderStateBadge($row['stato']);
                            ?>
                        </td>
                        <td class="project-state">
                            <?php if ($row['pagamento'] == "Cash") {
                                $val = "badge-danger";
                            } else {
                                $val = "badge-success";
                            } ?>
                            <span class="badge <?php echo $val; ?>"><?php echo $row['pagamento'] ?> </span>
                        </td>
                        <td class="project-actions text-right">
                            <a class="btn btn-primary btn-sm"
                               href="<?php echo base_url(); ?>index.php/dashboard/invoice/<?php echo $row['ordine']; ?>">
                                <i class="fas fa-folder">
                                </i>
                                Visualizza
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

</section>
