<div class="container">
    <header class="row">
        <h2 id="code">Codice: ____</h2>
    </header>

    <div class="row mt-1">
        <div id="cardContainer" class="col-12 align-self-center">
        <span id="indicazione" class="text-muted">Inserire i codici utilizzando direttamente la tastiera</span>
        </div>
    </div>
</div>

<script>
    var base_url = "<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url('js/') ?>readCode.js"></script>
<script src="<?php echo base_url('js/') ?>distributionAdmin.js"></script>
