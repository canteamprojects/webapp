<!-- Content Wrapper. Contains page  -->
<div class="content-wrapper">
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">
              <?php if(isset($breadcrumbId))
                        echo $this->breadcrumb->getTitle($breadcrumbId);
                    else
                        echo "Dashboard";
              ?>
          </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">

            <?php if(isset($breadcrumbId)) echo $this->breadcrumb->render($breadcrumbId); ?>

<!--          <ol class="breadcrumb float-sm-right">-->
<!--            <li class="breadcrumb-item"><a href="--><?php //site_url("dashboard/");?><!--">Home</a></li>-->
<!--            <li class="breadcrumb-item active">Dashboard</li>-->
<!--          </ol>-->

        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
