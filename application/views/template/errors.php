<?php
if(empty($errors)){
  return 0;
}
?>

<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert"
  aria-hidden="true">&times;</button>
  <h4><i class="icon fa fa-ban"></i> Attenzione!</h4>
  <?php
  foreach($errors as $errormessage){
    if( !empty($errormessage) ) { ?>
      <li><?= $errormessage ?></li>
    <?php }  //end foreach
  } ?>
</div>
