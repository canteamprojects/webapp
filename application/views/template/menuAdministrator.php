
<?php

$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$GLOBALS['actual_link']=$actual_link;
?>

<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php base_url();?>" class="brand-link">
      <img src="<?php echo base_url('images'); ?>/logo-def.jpg"  alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Canteen Booking</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
            <a href="<?php echo base_url(); ?>index.php/dashboard/orderListAdmin" class="nav-link">
                <i class="nav-icon fas fa-file-alt"></i>
                <p>
                    Ordini
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo base_url(); ?>index.php/dashboard/distributionAdmin" class="nav-link">
                <i class="nav-icon fas fa-fax"></i>
                <p>
                    Distribuzione
                </p>
            </a>
        </li>
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>index.php/dashboard/menuAdmin" class="nav-link">
              <i class="nav-icon fab fa-whmcs"></i>
              <p>
                Gestione Menu
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>index.php/dashboard/usersAdmin" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Utenti
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
