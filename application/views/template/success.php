<?php
if(empty($success)){
  return 0;
}
?>

<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert"
  aria-hidden="true">&times;</button>
  <h4><i class="icon fa fa-check "></i></h4>
  <?php
  foreach($success as $succMess){
    if( !empty($succMess) ) { ?>
      <li><?= $succMess ?></li>
    <?php }  //end foreach
  } ?>
</div>
