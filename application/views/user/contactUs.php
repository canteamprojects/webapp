<section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body row">
          <div class="col-5 text-center d-flex align-items-center justify-content-center">
            <div class="">
              <h2><strong>Canteen Booking</strong></h2>
              <p class="lead mb-5">Via Torino, 155, 30170 Mestre, Venezia VE<br>
                Telefono: +39 041 234 8411
              </p>
            </div>
          </div>
            <form action="<?php echo base_url(); ?>index.php/dashboard/contactUs" method="POST" class="col-7">
              <div>
                <div class="form-group">
                  <label for="inputSubject">Oggetto</label>
                  <input type="text" id="inputSubject" name="oggetto" class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="inputMessage">Messaggio</label>
                  <textarea id="inputMessage" class="form-control" name="messaggio" rows="4" required></textarea>
                </div>
                <div class="form-group">
                  <input type="submit" class="btn btn-primary" value="Invia email">
                </div>
              </div>
          </form>
        </div>
      </div>

    </section>
