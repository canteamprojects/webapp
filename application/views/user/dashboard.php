<?php declare(strict_types=1) ?>
<style>
    .text-block
    {
        padding-top: 7px;
        padding-bottom: 5px;
        background-color: rgba(0,0,0,.7);
    }
</style>

<section class="content">
    <div class="container-fluid">
        <div class="card card-success">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-xl-4">
                        <div class="card mb-4 containter">
                            <img class="card-img-top" src="<?php echo base_url(); ?>/images/dashboard1.jpg" alt="Profilo utente">
                            <div class="card-img-overlay d-flex flex-column justify-content-end ">

                            <div class="text-block">
                                <a  href="<?php echo base_url(); ?>index.php/dashboard/newOrder">
                                    <h5 class="card-title text-primary font-weight-bold">Nuovo ordine</h5>
                                </a>
                                <a  href="<?php echo base_url(); ?>index.php/dashboard/newOrder">
                                    <p class="card-text pb-1 pt-1 text-white">
                                        Salta la coda, scegli il menu e l'orario.
                                        In base al orario di ritiro prescelto potrai ricevere uno sconto speciale. Affrofittane adesso</p>
                                </a>
                            </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-xl-4">
                        <div class="card mb-4 containter">
                            <img class="card-img-top" src="<?php echo base_url(); ?>/images/dashboard2.jpg" alt="Profilo utente">
                            <div class="card-img-overlay d-flex flex-column justify-content-end">
                                <div class="text-block">
                                <a  href="<?php echo base_url(); ?>index.php/dashboard/profile">
                                    <h5 class="card-title text-primary font-weight-bold">Profilo</h5>
                                </a>
                                <a  href="<?php echo base_url(); ?>index.php/dashboard/profile">
                                    <p class="card-text pb-1 pt-1 text-white">
                                        Accedi al tuo profilo<br>
                                        Visualizza i tuoi dati e l'archivio dei tuoi ordini </p>
                                </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-xl-4">
                        <div class="card mb-4 containter">
                            <img class="card-img-top" src="<?php echo base_url(); ?>/images/dashboard3.jpg"
                                 alt="Contattaci">
                            <div class="card-img-overlay d-flex flex-column justify-content-end">
                                <div class="text-block">
                                <a href="<?php echo base_url(); ?>index.php/dashboard/contactUs">
                                    <h5 class="card-title text-primary font-weight-bold">Servizio
                                        clienti</h5>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/dashboard/contactUs">
                                    <p class="card-text pb-1 pt-1 text-white">
                                        Siamo qui per te<br>
                                        Contattaci </p>
                                </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
