<?php declare(strict_types=1);

require_once(APPPATH . '/helpers/ordersHelper.php');
use helpers\ordersHelper;
?>
<style>
    .bg-selected {
        background-color: #007bffc7 !important;
    }
    .user-select-none {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
        -o-user-select: none;
        user-select: none;
    }
</style>

<section class="content">
    <div id="tabella" class="container-fluid">
<?php if($selectDate) {?>
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <h5 class="card-header text-white">
                        Seleziona la data e l'ora dell'ordine
                    </h5>
                    <div class="card-body text-right">
                        <div id="datetimeInput" class="col-4 m-auto text-center">
                            <input id="inputDate" class="form-control mb-3" type="date" min="">

                            <div class="row justify-content-center mb-3">
                                <div class="col-auto">
                                    <input id="slotType" class="mr-auto" type="checkbox" data-toggle="toggle" data-on='<i class="fas fa-moon"></i> Cena' data-off='<i class="fas fa-sun"></i> Pranzo'>
                                </div>
                            </div>
                            <select id="inputSlotPranzo" class="form-control">
                                <?php foreach($roundsSlots as $slot) {
                                    if($slot->type == "pranzo") {
                                        $sconto = $slot->discount > 0 ? " | Sconto -" . ($slot->discount * 100) . "%" : "";
                                        echo '<option value=' . $slot->id . '>' . $slot->toString() . $sconto . '</option>';
                                    }
                                } ?>
                            </select>
                            <select id="inputSlotCena" hidden class="form-control">
                                <?php foreach($roundsSlots as $slot) {
                                    if($slot->type == "cena") {
                                        $sconto = $slot->discount > 0 ? " | Sconto -" . ($slot->discount * 100) . "%" : "";
                                        echo '<option value=' . $slot->id . '>' . $slot->toString() . $sconto . '</option>';
                                    }
                                } ?>
                            </select>
                            <br>
                            <a href="#" class="btn btn-primary" onclick="sendForm()">Vai</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php } else {?>
        <h3 id="date" style="font-weight: 300"></h3>
        <?php foreach($productCategories as $cat) { ?>

        <section class="content">
            <div class="card">
                <div class="card-header ">
                    <h5 class="text-white"><?php echo $cat->name;?></h5>
                </div>
                <div class="card-body">

                    <div class="row" >
                        <?php foreach($menuOfDay as $product) {
                            if($product['category'] == $cat->id) { ?>
                        <div class="col-auto" style="display: inline-block;">
                            <div class="card card-block text-right" onclick="productClick(<?php echo $product['id']; ?>)" style="background-color: rgba(0,0,0,.025)">
                                <div id="p_<?php echo $product['id']; ?>" class="card-body">
                                    <span class="user-select-none">
                                        <h5><?php echo $product['name']; ?></h5>
                                        <?php echo $product['price']; ?>&euro;<br>
                                    </span>
                                    <div hidden>
                                        <span class="user-select-none">x</span><br>
                                        <form class="form-inline" onsubmit="return false;">
                                            <span class="user-select-none ml-auto">Qty:</span>
                                            <input class="form-control form-control-sm ml-2" type="number" min="1" max="<?php echo $product['quantity']; ?>" value="1"/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } } ?>
                    </div>

                </div>
            </div>
        </section>
        <?php } ?>

        <div class="row">
            <div class="col-12">
                <!-- Main content -->
                <div class="invoice p-3 mb-3">

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Prodotti</th>
                                    <th>Prezzo Unitario</th>
                                    <th>Quantità</th>
                                    <th>Totale</th>
                                </tr>
                                </thead>
                                <tbody id="riepilogo">
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-6">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th style="width:50%">Totale parziale:</th>
                                        <td id="lilTotal">&euro;</td>
                                    </tr>
                                    <tr>
                                        <th>Time slot:</th>
                                        <td><?php echo $slot->toString(); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Sconto:</th>
                                        <td><?php echo $slot->discount*100.0; ?>%</td>
                                    </tr>
                                    <tr>
                                        <th>Totale:</th>
                                        <td id="bigTotal">&euro;</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- this row will not appear when printing -->
                    <div class="row no-print">
                        <div class="col-12">
<!--                            <a href="invoice-print.html" target="_blank" class="btn btn-default">-->
<!--                                <iclass="fas fa-print"></i> Stampa-->
<!--                            </a>-->
                            <!--<button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                                <i class="fas fa-download"></i> Generare PDF
                            </button>-->
                            <button type="button" id="sendOrder" disabled class="btn btn-success float-right" onclick="sendOrder()">
                                <i class="far fa-credit-card"></i> Invio Ordine
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.invoice -->
            </div><!-- /.col -->
        </div><!-- /.row -->
        <?php } ?>
    </div><!-- /.container-fluid -->
</section>

<div class="modal fade" id="modal-pickup">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Ritiro dell'ordine</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action='<?php echo base_url() ?>index.php/modal/pickUpOrder/' method="post">
                <input type="hidden" value='' name="orderId">
                <div class="modal-body">
                    <div>
                        L'ordine verrà segnato come ritirato.
                    </div>
                    <div>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                    <input type="submit" class="btn btn-default" value="Conferma">
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    'use strict';
    function sendForm()
    {
        let date = $("#inputDate").val();
        let slot = $("#slotType").prop('checked') ? $("#inputSlotCena").val() : $("#inputSlotPranzo").val();
        window.location.href = "<?php echo base_url(); ?>index.php/dashboard/newOrder/"+date+"/"+slot;
    }

    let days = [
        "Domenica",
        "Lunedì",
        "Martedì",
        "Mercoledì",
        "Giovedì",
        "Venerdì",
        "Sabato",
    ];
    function printDate()
    {
        let literalDate = "<?php echo $date ?? ""; ?>";
        let date = new Date(literalDate);

        let month = '' + (date.getMonth() + 1);
        let day = '' + date.getDate();
        let year = date.getFullYear();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        let dayName = literalDate === today() ? "Oggi" : days[date.getDay()];

        $("#date").text(day + "-" + month + "-" + year + ", " + dayName);
    }

    function today()
    {
        let date = new Date();
        return date.toISOString().split("T")[0];
    }
    function todayPlusOneWeek()
    {
        let date = new Date();
        date.setDate(date.getDate()+7);
        return date.toISOString().split("T")[0];
    }

    function productClick(id) {
        if(event.target.nodeName !== "INPUT") {
            let $body = $("#p_" + id);
            let $qty = $("#p_" + id + " > div");
            let $input = $("#p_" + id + "> input");

            $body.toggleClass("clicked");
            if ($body.hasClass("clicked"))
            {
                $body.parent().addClass("bg-selected");
                $body.parent().addClass("text-white");
                $qty.removeAttr("hidden");
            }
            else
            {
                $body.parent().removeClass("bg-selected");
                $body.parent().removeClass("text-white");
                $qty.attr("hidden", "");
            }
        }
        redraw();
    }

    <?php if(!$selectDate) {?>
    function getOrder() {
        let order = Object();
        order.items = [];
        order.total = 0;
        order.date = "<?php echo $date; ?>";
        order.slot = <?php echo $slot->id; ?>;

        let id = 0;
        let $body;
        let $input;
        let item;

        <?php foreach($menuOfDay as $product) { ?>
        id = <?php echo $product['id']; ?>;
        $body = $("#p_" + id);
        $input = $("#p_" + id + " input");

        if($body.hasClass("clicked"))
        {
            item = Object();
            item.id = <?php echo $product['id']; ?>;
            item.name = "<?php echo $product['name']; ?>";
            item.qty = $input.val();
            item.price = <?php echo $product['price']; ?>;
            item.total = item.price*item.qty;

            order.items.push(item);
            order.total += item.total > 0 ? (item.total) : 0;
        }
        <?php }?>

        order.discount = <?php echo $slot->discount; ?>;
        order.totalWithDiscount = order.total - (order.total*order.discount);

        return order;
    }

    function redraw() {
        let html = "";
        let order = getOrder();


        $("#sendOrder").attr("disabled","");
        for(let i in order.items)
        {
            $("#sendOrder").removeAttr("disabled");

            let item = order.items[i];
            html += "<tr>";
            html += "<td>" + item.name + "</td>";
            html += "<td>" + item.price + "&euro;</td>";
            html += "<td>" + item.qty + "</td>";
            html += "<td>" + item.total + "&euro;</td>";
            html += "</tr>";
        }

        $("#riepilogo").html(html);
        $("#lilTotal").text(order.total.toFixed(2)+"€");
        $("#bigTotal").text(order.totalWithDiscount.toFixed(2)+"€");
    }

    function sendOrder() {
        $("#sendOrder").attr("disabled","");
        $.ajax({
            contentType: 'application/json',
            data: JSON.stringify(getOrder()),
            dataType: 'json',
            complete: function(){
                window.location.href = "<?php echo base_url(); ?>index.php/dashboard/profile";
            },
            type: 'POST',
            url: '<?php echo base_url(); ?>index.php/dashboard/postOrder'
        });
    }
    <?php } ?>

    $(function ()
    {
        <?php if($selectDate) {?>
            $('#datetimeInput > input[type="date"]')
                .attr('value', today())
                .attr('min', today())
                .attr('max', todayPlusOneWeek());

            $("#slotType").change(function() {
                if($("#slotType").prop('checked'))
                {
                    $("#inputSlotCena").removeAttr("hidden");
                    $("#inputSlotPranzo").attr("hidden", "");
                }
                else
                {
                    $("#inputSlotCena").attr("hidden", "");
                    $("#inputSlotPranzo").removeAttr("hidden");
                }
            });
        <?php } else { ?>
            printDate();
        <?php } ?>
    });
</script>