<?php declare(strict_types=1) ?>
    <?php
require_once(APPPATH . '/helpers/ordersHelper.php');

use helpers\ordersHelper;

?>
<section class="content">
    <div id="tabella" class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Main content -->
                <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-12">
                            <h4>
                                <i class="fas fa-globe"></i> Canteen Booking
                                <small class="float-right">Data: <?php
                                    echo $order->date;
                                    echo " ";
                                    echo $order->startTime; ?></small>
                            </h4>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                            Cliente:
                            <address>
                                <strong> <?php echo $order->name;
                                    echo " ";
                                    echo $order->surname; ?></strong><br>
                                Indirizzo: <br>
                                <?php
                                $pos = strpos($order->indirizzo, ",");
                                $parte1 = substr($order->indirizzo, 0, $pos);
                                $parte2 = substr($order->indirizzo, $pos);
                                $str = substr($parte2, 1);
                                echo $parte1; ?> <br>
                                <?php echo $str; ?><br>
                                Telefono: <?php echo $order->telefono; ?><br>
                                Email: <?php echo " ";
                                echo $order->email; ?>
                            </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            <b>N. Ordine #<?php echo $orderId; ?></b><br>
                            <b>Codice Segreto: <?php echo $order->code; ?></b><br>
                            <b>Stato dell'ordine:</b> <?php echo ordersHelper::orderStateBadge($order->state); ?>

                            <?php
                            if ($canPickup) {
                                ?>
                                <br>
                                <br>
                                <button type="button"
                                        class="btn btn-success" data-toggle="modal"
                                        style="margin-right: 5px;"
                                        data-target="#modal-pickup">
                                    <i class="fas fa-shopping-bag" style="margin-right: 8px"></i>
                                    Ritiro Ordine
                                </button>
                            <?php } ?>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Prodotti</th>
                                    <th>Quantità</th>
                                    <!--<th>Descrizione</th>-->
                                    <th>Prezzo Unitario</th>
                                    <th>Totale</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $totaleParziale = 0;
                                foreach ($items as $item) { ?>
                                    <tr>
                                        <td><?php echo $item['productName']; ?></td>
                                        <!--<td>Per la descrizione si dovrebbe aggiungere il campo in database e la modalità
                                            di inserimento lato amministratore
                                        </td>-->
                                        <td><?php echo $item['quantity']; ?></td>
                                        <td><?php echo $item['unitaryPrice']; ?>&euro;</td>
                                        <td><?php echo $item['total']; ?>&euro;</td>

                                    </tr>
                                    <?php $totaleParziale = $totaleParziale + $item['total'];
                                } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-6">
                            <p class="lead">Importo</p>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th style="width:50%">Totale parziale:</th>
                                        <td><?php echo number_format($totaleParziale, 2); ?>&euro;</td>
                                    </tr>
                                    <tr>
                                        <th>Time slot:</th>
                                        <td><?php echo $order->startTime; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Sconto:</th>
                                        <td><?php echo $order->discount * 100; ?>&#37;</td>
                                    </tr>
                                    <tr>
                                        <th>Totale:</th>
                                        <td><?php echo $order->total; ?>&euro;</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- this row will not appear when printing -->
                    <div class="row no-print">
                        <div class="col-12">
                         
                            <button onclick="window.print();"  type="button" class="btn  float-right" style="margin-right: 5px;">
                                <i class="fas fa-print"></i> Stampa
                            </button>
                            <?php
                            if ($canPay) {
                                ?>
                                <button type="button" class="btn btn-success float-right"
                                        data-toggle="modal"
                                        style="margin-right: 5px;"
                                        data-target="#modal-credit-card">
                                    <i class="far fa-credit-card"></i>
                                    Invio Pagamento
                                </button>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <!-- /.invoice -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>

<div class="modal fade" id="modal-pickup">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Ritiro dell'ordine</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action='<?php echo base_url() ?>index.php/modal/pickUpOrder/' method="post">
                <input type="hidden" value='<?= $orderId ?>' name="orderId">
                <div class="modal-body">
                    <div>
                        L'ordine verrà segnato come ritirato.
                    </div>
                    <div>
                        <?php
                        if ($order->state == 'Confirmed')
                            echo "Non ancora pagato.";
                        ?>

                        <?php
                        if ($order->state == 'Paid')
                            echo "Pagamento ricevuto.";
                        ?>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                    <input type="submit" class="btn btn-default" value="Conferma">
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-credit-card">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Inserisci i dati della carta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action='<?php echo base_url() ?>index.php/modal/payWithCreditCard/' method="post" role="form">
                <input type="hidden" value='<?= $orderId ?>' name="orderId">
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Dati proprietario</label>
                                    <input name="owner" type="text" class="form-control" placeholder="Nome e cognome" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Numero della carta</label>
                                    <div class="input-group">
                                        <input name="ccn" type="tel" class="form-control" inputmode="numeric" pattern="[0-9\-]{13,19}" maxlength="19" placeholder="xxxx-xxxx-xxxx-xxxx">
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label><span class="hidden-xs">Scadenza</span></label>
                                    <input name="exp" type="tel" class="form-control" inputmode="numeric" pattern="[0-9\/\-]{7}" maxlength="7" placeholder="MM/YYYY">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Codice CVC</label>
                                    <input name="cvc" type="tel" class="form-control" inputmode="numeric" pattern="[0-9]{3,4}" maxlength="4" placeholder="CVC" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                    <input type="submit" class="btn btn-default" value="Conferma">
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
