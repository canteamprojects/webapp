<?php
require_once(APPPATH . '/helpers/ordersHelper.php');
require_once(APPPATH . '/helpers/usersHelper.php');

use helpers\ordersHelper;
use helpers\usersHelper;

?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <?php
                            echo usersHelper::userPictureOrEmpty($user->immagineBase64);
                            ?>
                        </div>

                        <!--<h3 class="profile-username text-center">Nina Mcintire</h3>-->
                        <h3 class="profile-username text-center"><?php echo $user->login; ?></h3>

                        <p class="text-muted text-center">Studente</p>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Ordini effettuati</b> <a class="float-right"><?php echo $user->insertedOrders; ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Ordini da ritirare</b> <a
                                        class="float-right"><?php echo $user->confirmedOrders; ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Ordini ritirati</b> <a class="float-right"><?php echo $user->pickedUpOrders; ?></a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- About Me Box -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Informazioni personali</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <strong><i></i> Nome e Cognome</strong>

                        <p class="text-muted"><?php echo $user->name; ?> <?php echo $user->surname; ?></p>
                        <hr>
                        <strong><i></i> Numero di telefono</strong>

                        <p class="text-muted"><?php echo $user->telefono; ?></p>
                        <hr>
                        <strong><i></i> Data di Nascita</strong>
                        <p class="text-muted"><?php echo $user->dataDiNascita; ?></p>
                        <hr>
                        <strong><i></i> Indirizzo</strong>

                        <p class="text-muted"><?php echo $user->indirizzo; ?></p>

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title text-white">Ordini</h3>

                      
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-striped projects">
                            <thead>
                            <tr>
                                <th style="width: 1%">
                                    #
                                </th>
                                <th style="width: 20%">
                                    Ordine
                                </th>
                                <th style="width: 30%">
                                    Dettagli
                                </th>
                                <th style="width: 8%" class="text-center">
                                    Stato
                                </th>
                                <th style="width: 20%">
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($orders as &$order) { ?>
                                <tr>
                                    <td>
                                        #
                                    </td>
                                    <td>
                                        <a>
                                            Ordine <?php echo $order['id']; ?>
                                        </a>
                                        <br>
                                        <small>
                                            Data <?php
                                            echo date_format(date_create($order['orderDate']), "d/m/Y");
                                            echo " ";
                                            echo date_format(date_create($order['startTime']), "h:i");
                                            echo "-";
                                            echo date_format(date_create($order['endTime']), "h:i");

                                            ?>
                                        </small>
                                    </td>
                                    <td>
                                        <a>
                                            <?php echo $order['details']; ?>
                                        </a>
                                    </td>
                                    <td class="project-state">
                                        <?php
                                        echo ordersHelper::orderStateBadge($order['state']);
                                        ?>
                                    </td>
                                    <td class="project-actions text-right">
                                        <a class="btn btn-primary btn-sm"
                                           href="<?php echo base_url(); ?>index.php/dashboard/invoice/<?php echo $order['id']; ?>">
                                            <i class="fas fa-folder">
                                            </i>
                                            Visualizza
                                        </a>
                                        <!--<a class="btn btn-info btn-sm" href="#">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                            Modifica
                                        </a>
                                        <a class="btn btn-danger btn-sm" href="#">
                                            <i class="fas fa-trash">
                                            </i>
                                            Elimina
                                        </a>-->
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
