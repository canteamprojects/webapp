
<table cellspacing="0" cellpadding="10" style="width: 100%; height: auto; border: 0;">
    <tr style="height: 10%"></tr>

    <tr>
        <td style="width: 10%;"></td>
        <td style="width: 80%; background-color: #d9d9d9; text-align: center">
            <img src="https://i.ibb.co/hD77r5v/favicon-T.png" style="max-height: 100px">
        </td>
        <td style="width: 10%;"></td>
    </tr>
    <tr>
        <td style="width: 10%;"></td>
        <td style="width: 80%; background-color: #d9d9d9; text-align: center">
            <h1>Canteen Reservation</h1>
        </td>
        <td style="width: 10%;"></td>
    </tr>
    <tr>
        <td style="width: 10%;"></td>
        <td style="width: 80%; background-color: #d9d9d9; text-align: center">
            <h3 style="margin-bottom: 10px;font-weight: 200;">Ecco il codice con cui ritirare il Suo ordine:</h3>
            <table cellspacing="0" cellpadding="10" style="width: 100%; height: auto; border: 0;">
                <tr>
                    <td style="width: 40%;"></td>
                    <td style="width: 20%; background-color: #000000; text-align: center">
                        <h1 style="color: #ffffff; margin: 0"><?php echo $code; ?></h1>
                    </td>
                    <td style="width: 40%;"></td>
                </tr>

            </table>
        </td>
        <td style="width: 10%;"></td>
    </tr>

    <tr style="height: 10%"></tr>
</table>
