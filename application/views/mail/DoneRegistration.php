
<table cellspacing="0" cellpadding="10" style="width: 100%; height: auto; border: 0;">

    <tr>
        <td style="width: 100%; background-color: #d9d9d9; text-align: center">
            <img src="https://i.ibb.co/hD77r5v/favicon-T.png" style="max-height: 100px">
        </td>
    </tr>
    <tr>
        <td style="width: 100%; background-color: #d9d9d9; text-align: center">
            <h1>Canteen Reservation</h1>
        </td>
    </tr>
    <tr>
        <td style="width: 100%; background-color: #d9d9d9; text-align: center">
            <h3 style="margin-bottom: 10px;font-weight: 200;">Complimenti ora è registrato al nostro sistema, può accedere con il nome utente seguente:</h3>
            <table cellspacing="0" cellpadding="10" style="width: 100%; height: auto; border: 0;">
                <tr>
                    <td style="width: 40%;"></td>
                    <td style="width: 20%; background-color: #000000; text-align: center">
                        <h1 style="color: #ffffff; margin: 0"><?php echo $username; ?></h1>
                    </td>
                    <td style="width: 40%;"></td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="10" style="width: 100%; height: auto; border: 0; margin-top: 100px; margin-bottom: 100px">
                <tr>
                    <td style="width: 30%;"></td>
                    <td style="width: 40%; text-align: center; background-color: #007bff">
                        <a href="<?php echo base_url(); ?>index.php/login?username=<?php echo $username; ?>" style="text-decoration: none">
                            <h1 style="color: #ffffff; margin: 0">Accedi Ora</h1>
                        </a>
                    </td>
                    <td style="width: 30%;"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
