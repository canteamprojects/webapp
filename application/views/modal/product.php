<?php if (isset($product["productId"]) && $product["productId"]) :?>
<input name="id" type="hidden" value="<?= $product["productId"]?>">
<?php
  endif;
  error_reporting(E_ALL & ~E_NOTICE);
?>
<div class="row">
  <div class="col-sm-12">
    <!-- text input -->
    <div class="form-group">
      <label>Categoria</label>
      <select required name="categoryId" class="form-control col-sm-12">
        <?= $categorySelect?>
      </select>
    </div>
    <div class="form-group">
      <label>Nome</label>
      <input required name="productName" type="text" class="form-control" placeholder="Inserisci Nome" value="<?= $product["productName"]?>">
    </div>
    <div class="form-group">
      <label>Prezzo</label>
      <input required name="price" type="text" class="form-control" placeholder="Inserisci Prezzo" value="<?= $product["price"]?>">
    </div>
    <div class="form-group">
      <label>Quantità</label>
      <input required name="quantity" type="text" class="form-control" placeholder="Inserisci Quantità" value="<?= $product["quantity"]?>">
    </div>
  </div>
</div>
