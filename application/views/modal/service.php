<?php if (isset($service["roundId"]) && $service["roundId"]) :?>
<input name="id" type="hidden" value="<?= $service["roundId"]?>">
<?php endif; ?>
<div class="row">
  <div class="col-sm-12">
    <!-- text input -->
    <div class="form-group">
      <div class="form-group">
        <label>Data</label>
        <input required name="date" type="date" class="col-sm-6 form-control" value="<?php if (isset($service["date"])) echo $service["date"]?>">
      </div>
      <label>Piatti</label>
      <select required id ="products" name="products[]" class="select2" multiple="multiple" data-placeholder="Seleziona i Piatti" style="width: 100%;">
        <?= $productsSelect ?>
      </select>
    </div>
  </div>
</div>
